/// io.dart
library;

// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:
//
import 'dart:io';

import '../debug.dart';
import '../osclib.dart';

class OSCSocket {
  final InternetAddress destination;
  final int destinationPort;

  final InternetAddress serverAddress;
  final int serverPort;

  InternetAddress lastMessageAddress;
  int lastMessagePort;

  RawDatagramSocket _socket;

  OSCSocket({
    this.destination,
    this.destinationPort,
    this.serverAddress,
    this.serverPort,
  });

  int get localport => _socket.port;

  void close() {
    _socket.close();
  }

  Future<RawDatagramSocket> setupSocket() async {
    var address = serverAddress ?? InternetAddress.anyIPv4;
    var port = serverPort ?? 0;
    RawDatagramSocket socket;
    socket = await RawDatagramSocket.bind(address, port);
    return socket;
  }

  Future<void> setup() async {
    _socket ??= await setupSocket();
  }

  /// RawDatagramSockets don't support onDone, onError callbacks
  /// because UDP has no concept of a "connection" that can be closed.
  Future<void> listen(void Function(OSCMessage msg) onData) async {
    _socket.listen((e) {
      final datagram = _socket.receive();
      if (datagram != null) {
        lastMessageAddress = datagram.address;
        lastMessagePort = datagram.port;
        final msg = OSCMessage.fromBytes(datagram.data);
        // print('OSCsocket UDP listen received { $msg }');
        onData(msg);
      }
    });
  }

  Future<int> send(OSCMessage msg) async {
    _socket.writeEventsEnabled = true;
    var to = destination ?? lastMessageAddress;
    var port = destinationPort ?? lastMessagePort;
    var sent = _socket.send(msg.toBytes(), to, port);
    dPrint('OSCsocket UDP send $sent $msg');
    return sent;
  }

  Future<int> reply(OSCMessage msg) async {
    return _socket.send(msg.toBytes(), lastMessageAddress, lastMessagePort);
  }

  @override
  String toString() =>
      'destaddr $destination, destport $destinationPort, serveraddr $serverAddress, serverport $serverPort, '
      'lastaddr $lastMessageAddress, lastport $lastMessagePort, socket ${_socket.hashCode}';
}
