/// size_config.dart: handle screen size/resolution
library;

// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:
//
import 'package:flutter/widgets.dart';

import 'debug.dart';

// use this:
// @override
// Widget build(BuildContext context) {
// // add this at the top of the widget:
//   SizeConfig().init(context);
//
// then use it like this:
//  child: Container(
//   height: SizeConfig.heightMultiplier * 20,
//   width: SizeConfig.widthMultiplier * 50,
//   color: Colors.orange,
//  ),

/// SizeConfig: collect and manage the device dependent sizes.
class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static Orientation orientation;
  static bool isPortrait;
  static bool isMobilePortrait;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  static double textMultiplier;
  static double imageSizeMultiplier;
  static double heightMultiplier;
  static double widthMultiplier;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    orientation = _mediaQueryData.orientation;

    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;

    if (orientation == Orientation.portrait) {
      isPortrait = true;
      isMobilePortrait = (screenWidth < 450);
    } else {
      isPortrait = false;
      isMobilePortrait = false;
    }

    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;

    textMultiplier = safeBlockVertical;
    imageSizeMultiplier = safeBlockHorizontal;
    heightMultiplier = safeBlockVertical;
    widthMultiplier = safeBlockHorizontal;

    dPrint('$this');
  }

  @override
  String toString() =>
      'SizeConfig screenWidth $screenWidth screenHeight $screenHeight\n'
      'SizeConfig textMultiplier $textMultiplier imageSizeMultiplier $imageSizeMultiplier\n'
      'SizeConfig heightMultiplier $heightMultiplier widthMultiplier $widthMultiplier\n'
      'SizeConfig isPortrait $isPortrait isMobilePortrait $isMobilePortrait orientation $orientation';
}
