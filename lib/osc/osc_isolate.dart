// osc_isolate - setup isolate for UDP I/O and codecs
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:synchronized/synchronized.dart';

import '../debug.dart';
import '../osc/osc.dart';
import '../osclib.dart';

// one common isolate for recv, send.  UDP port is unique, requires one socket.

/// OSCIsolate(isolate, name, recvPort): Isolate class.  One for all I/O, handles both send and receive.
// sendport is not needed by reader, as it never writes.
class OSCIsolate {
  OSCIsolate({this.isolate, this.name, this.recvPort});

  Isolate isolate;
  final String name;
  final ReceivePort recvPort;

  SendPort workerPort;
  SendPort mainPort;
  Capability pauseCapability;

  set mainSendPort(SendPort sp) {
    mainPort = sp;
  }

  set workerSendPort(SendPort sp) {
    workerPort = sp;
  }

  /// send msg to worker workerPort if port not null.
  void send(dynamic msg) {
    workerPort.send(msg);
  }

  @override
  String toString() {
    return 'OSCIsolate { Isolate: ${isolate.hashCode}, String: $name, '
        'ReceivePort: ${recvPort.hashCode}, workerPort: ${workerPort.hashCode}, '
        'mainPort: ${mainPort.hashCode}, Capability: $pauseCapability}';
  }
}

/// setupIsolate(name, worker): Create an isolate at global level.
Future<OSCIsolate> setupIsolate(String name, Function worker) async {
  var recvPort = ReceivePort();

  // spawn the isolate with the initial sendport
  var isolate = await Isolate.spawn(worker, recvPort.sendPort);

  // instantiate an OSC Isolate with name and recvport
  return OSCIsolate(isolate: isolate, name: name, recvPort: recvPort);
}

// nasty globals because I haven't figured out how to make the worker a class yet.
String _sendAddress;
String _sendPort;

/// socketSetup(sock): setup the UDP socket for read/write.
Future<OSCSocket> socketSetup(OSCSocket sock) async {
  var socket = sock;
  bool validAddress;
  // converted _sendAddress
  List<InternetAddress> iAddresses;
  // converted _sendPort
  int iPort;

  socket.close();

  // lookup address
  try {
    iAddresses = await InternetAddress.lookup(_sendAddress,
        type: InternetAddressType.IPv4);
    if (iAddresses.isNotEmpty && iAddresses[0].rawAddress.isNotEmpty) {
      validAddress = true;
      iPort = int.parse(_sendPort);
    } else {
      validAddress = false;
      return null;
    }
  } on SocketException catch (e) {
    print('osc socketSetup address exception $e');
    validAddress = false;
    return null;
  }

  if (validAddress) {
    // open UDP socket on anyIPv4.  Use random port, save the value.
    socket = OSCSocket(destination: iAddresses[0], destinationPort: iPort);
    await socket.setup();
  } else {
    print('osc socketSetup invalid address $_sendAddress');
  }
  return socket;
}

/// _ioWorker(sendPort): entry point for the reader/writer isolate.
/// listen on recvPort, udpSocket.
/// encode/decode messages, pass on to other isolate or network.
Future<void> _ioWorker(SendPort sendPort) async {
  // this is opened uninitialized, needs a socket send/receive to set addresses... hmm
  OSCSocket udpSocket;
  var lock = Lock(); // let the settings settle before sending.

  // Open a new ReceivePort for incoming messages.
  var recvPort = ReceivePort();

  // Notify our creator what port to use to send to this isolate.
  // the recvPort/sendPort from the creator will close after receiving the worker sendport.
  // this isolate loop on the recvPort it created.
  sendPort.send(recvPort.sendPort);

  // setup UDP OSCSocket to send UDP messages to _sendAddress:_sendPort with random port.
  //
  // loop on outbound stream, encode messages, send to OSC
  // listen on UDP too.
  recvPort.listen((msg) async {
    // handle set address, setup udpSocket
    await lock.synchronized(() async {
      if (msg is SendMsgType && msg.messageType == SendMsgType.NewAddress) {
        _sendAddress = msg.value.address;
        _sendPort = msg.value.port;
        print('ioworker Settings $_sendAddress $_sendPort');

        // close if already opened.  New address.
        udpSocket = await socketSetup(udpSocket);
      }
    });

    // dPrint('osc_isolate ioworker recvport listen ${msg.runtimeType}:$msg');

    // check for incoming UDP, forward to main isolate.
    // start listening if address has been (re)set.
    if (msg is SendMsgType && msg.messageType == SendMsgType.NewAddress) {
      print('ioworker start listening $_sendAddress $_sendPort');
      // UDP ioworker receive method listener
      // send decoded message to main isolate for state changes.
      await udpSocket.listen((umsg) {
        sendPort.send(umsg);
        dPrint(umsg);
      });
    }
    if (msg is SendMsgType) {
      if (msg.messageType != SendMsgType.NewAddress) {
        // encode message into OSCMessage.
        var address = SendMsgType(msg.messageType).toAddress();
        // SendMsgArguments keeps model, returns list of objects
        // could handle optional ssid, value
        var arguments =
            SendMsgArguments(msg.messageType, ssid: msg.ssid, value: msg.value);
        // UDP send method
        // print('osc_isolate ioworker SendMsgType $msg $arguments');
        await udpSocket.send(OSCMessage(address, arguments: arguments));
      }
    } else {
      print('osc ioworker: unknown type: $msg');
    }
  }, onDone: () {
    print('osc ioworker: done');
  });
}

/// OSCComm(onData): ask worker to encode and send messages to OSC server
// and receive and decode OSC messages for App.
// needs to know the sendport the worker is listening to.
// TODO: move the listen to the isolate bloc or not.
Future<OSCIsolate> oscComm({onData}) async {
  OSCIsolate isolate;

  isolate ??= await setupIsolate('readerwriter', _ioWorker);

  // save this, it goes into the send msg.
  isolate.mainSendPort = isolate.recvPort.sendPort;

  // the isolate sends it's SendPort as the first message

  // handle received messages until done
  // process the message from UDP->OSC decode; ignore the first sendport message
  // the oscRecv isolate only listens, never sends
  // the intent is to update state in the UI, this method runs in the main isolate.
  // TODO: handle Ardour state changes.
  // TODO: does the listen/ondata belong here or should it be moved up?
  isolate.recvPort.listen((msg) {
    // dPrint('oscComm listen ${msg.runtimeType}:$msg');
    if (msg is OSCMessage) {
      // Handle OSCMessage
      onData(msg);
    } else if (msg is SendPort) {
      isolate.workerSendPort = msg;
    } else {
      print('osc oscComm unknown: ${msg.runtimeType}: $msg');
    }
  }, onDone: () {
    print('osc oscComm: listen done');
  });

  return isolate;
}
