/// osc_ardour : Ardour6 Messages
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import '../debug.dart';

// Ardour classes for OSC communication
// can't use enum because the values matter to ardour

/// ReplySsid is a singleton class used to track the SSID in the /reply messages.  Ardour6 has a bug.
/// The AudioBus /reply B has 0 for the SSID.
// The ReplySsid class can be reset to zero, incremented, and the current value returned.
class ReplySsid {
  static final ReplySsid _singleton = ReplySsid._internal();
  int _ssid;

  /// Singleton Factory interface.
  factory ReplySsid() {
    return _singleton;
  }
  ReplySsid._internal() {
    _ssid = 0;
  }
  void increment() {
    _ssid += 1;
  }

  void reset() {
    _ssid = 0;
  }

  int get ssid => _ssid;
}

/// StripType sets the bits for /set_surface that select the types of strips to feedback.
class StripType {
  static final StripType _singleton = StripType._internal();
  int stripType;

  static const AudioTrack = 1;
  static const MidiTrack = 2;
  static const AudioBus = 4;
  static const MidiBus = 8;
  static const Vca = 16;
  static const Master = 32;
  static const Monitor = 64;
  static const Foldback = 128;
  static const Selected = 256;
  static const Hidden = 512;
  static const UseGroup = 1024;

  /// Singleton Factory interface.
  factory StripType() {
    return _singleton;
  }

  StripType._internal() {
    // set the stripType bits
    stripType = AudioTrack +
        MidiTrack +
        AudioBus +
        MidiBus +
        Vca +
        // Master +
        // Monitor +
        Foldback +
        Selected +
        // StripType.Hidden +
        UseGroup;
  }

  @override
  String toString() => '$stripType';
}

/// Feedback sets the bits for /set_surface that select the feedback desired.
class Feedback {
  static final Feedback _singleton = Feedback._internal();
  int feedback;

  static const ButtonStatus = 1;
  static const StripControl = 2;
  static const SsidPath = 4;
  static const Heartbeat = 8;
  static const MasterSection = 16;
  static const BarBeat = 32;
  static const TimeCode = 64;
  static const MeterDb = 128;
  static const Meter16Bit = 256;
  static const SignalPresent = 512;
  static const PositionSample = 1024;
  static const PositionTime = 2048;
  static const SelectChannel = 8192;
  static const SlashReply = 16384;

  /// Singleton Factory interface.
  factory Feedback() {
    return _singleton;
  }

  Feedback._internal() {
    // set the feedback bits
    feedback = ButtonStatus +
        StripControl +
        // SsidPath +
        Heartbeat +
        MasterSection +
        // BarBeat +
        // TimeCode +
        MeterDb +
        // Meter16Bit +
        // SignalPresent +
        // PositionSample +
        PositionTime +
        // SelectChannel +
        SlashReply;
  }

  @override
  String toString() => '$feedback';
}

/// SetSurface collects all the values for the /set_surface message.
/// /set_surface bank_size strip_types feedback fadermode send_page_size plugin_page_size port linkset linkid
/// 0. bank size is the number of channel strips the controller supports without banking. Setting this to 0 turns banking off by setting the bank size to infinite.
/// 1. strip_types is an integer made up of bits.
/// 2. feedback is an integer made up of bits.
/// 3. fadermode is an int. We will use mode 2 and receive both /strip/fader ssid and /strip/gain ssid.
/// 4. send_page_size is an int for the number of send channels that can be controlled at one time. Each channel has a name, level and enable control.
/// 5. plugin_page_size is an int for the number of plugin controls that can be controlled at one time.
/// 6. port is the port the controller would like to receive it's feedback on.  The value for port can be 0 for auto port mode or any port value above 1024.   Default port is 8000.
/// linkset and linkid are not needed for this app at this time and do not need to be sent.
class SetSurface {
  static final SetSurface _singleton = SetSurface._internal();
  List<Object> arguments = []; // List(7);

  SetSurface._internal() {
    arguments.length = 7;
    arguments[0] = 0; // bank_size
    arguments[1] = StripType().stripType;
    arguments[2] = Feedback().feedback;
    arguments[3] = 2; // fadermode
    arguments[4] = 0; // send_page_size
    arguments[5] = 0; // plugin_page_size
    arguments[6] = 0; // port
  }

  /// Singleton Factory interface.
  factory SetSurface() {
    return _singleton;
  }

  @override
  String toString() => '$arguments';
}

/// SendMsgType: define constant values for send messages for efficiency.
class SendMsgType {
  final int messageType;
  final int ssid; // this can be null for messages without ssid
  final dynamic value; // this can be null for messages without value.
  SendMsgType(this.messageType, {this.ssid, this.value});

  static const NewAddress = 1;
  static const StripList = 2;
  static const SetSurface = 3;
  static const GotoStart = 4;
  static const GotoEnd = 5;
  static const Stop = 6;
  static const Play = 7;
  static const RecEnable = 8;
  static const SelectExpand = 9;
  static const Fader = 10;
  static const Gain = 11;
  static const Mute = 12;
  static const Solo = 13;
  static const StripRecEnable = 14;
  static const MasterFader = 15;
  static const MasterGain = 16;
  static const MasterMute = 17;

  static const msgTypeAsString = [
    '/settings',
    '/strip/list',
    '/set_surface',
    '/goto_start',
    '/goto_end',
    '/transport_stop',
    '/transport_play',
    '/rec_enable_toggle',
    '/select/expand',
    '/strip/fader',
    '/strip/gain',
    '/strip/mute',
    '/strip/solo',
    '/strip/recenable',
    '/master/fader',
    '/master/gain',
    '/master/mute',
  ];

  String toAddress() => msgTypeAsString[messageType - 1];

  @override
  String toString() => '$messageType ${msgTypeAsString[messageType - 1]}';
}

/// SendMsgArguments: setup send message arguments based on argumentType.
/// if ssid is supplied, it is put in the return list as the first value.
/// if value is supplied, it is put in the return list as the only value.
List<Object> SendMsgArguments(int argumentType, {int ssid, dynamic value}) {
  var arguments = <Object>[];

  // no ssid
  switch (argumentType) {
    case SendMsgType.StripList:
      ReplySsid().reset();
      return arguments;
    case SendMsgType.GotoStart:
    case SendMsgType.GotoEnd:
    case SendMsgType.Stop:
    case SendMsgType.Play:
    case SendMsgType.RecEnable:
      return arguments;
    case SendMsgType.SetSurface:
      // set_surface bank_size strip_types feedback fadermode send_page_size plugin_page_size port linkset linkid
      arguments = SetSurface().arguments;
      return arguments;

    case SendMsgType.MasterFader:
    case SendMsgType.MasterGain:
      arguments.add(value); // no ssid
      return arguments;

    case SendMsgType.MasterMute:
      arguments.add(value ? 1 : 0); // boolean to int, no ssid
      return arguments;
  }

  arguments.add(ssid);
  switch (argumentType) {
    case SendMsgType.SelectExpand:
    case SendMsgType.Fader:
    case SendMsgType.Gain:
      arguments.add(value);
      return arguments;

    case SendMsgType.Mute:
    case SendMsgType.Solo:
    case SendMsgType.StripRecEnable:
      arguments.add(value ? 1 : 0); // boolean to int
      return arguments;

    default:
      print('SendMsgArguments unknown type $argumentType');
      break;
  }
  dPrint('osc sendmsgarguments ${SendMsgType(argumentType)} -> $arguments');
  return arguments;
}
