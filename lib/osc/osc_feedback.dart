/// osc_feedback - OSC Messages from Ardour
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import '../blocs/blocs.dart';

import '../debug.dart';
import '../osclib.dart';
import './osc_ardour.dart';

// Ardour OSC feedback messages
// handle these transport messages.
//
// OSCMessage(/heartbeat, args: [0.0])
// OSCMessage(/position/time, args: [00:00:00.000])
// OSCMessage(/rec_enable_toggle, args: [0.0])
// OSCMessage(/session_name, args: [CarlaArdourJamulus1])
// OSCMessage(/transport_play, args: [0.0])
// OSCMessage(/transport_stop, args: [1.0])
//
// Handle these feedback messages
// OscDecode /master/meter [0.48729193210601807]
// OscDecode /master/fader [0.48729193210601807]
// OSCDecode /master/gain  [-3.9600067138671875])
// OscDecode /strip/meter [1, 0.5108038783073425]
// OscDecode /strip/fader [1, 0.5108038783073425]
// OscDecode /strip/gain  [2, 1.755035161972046])
// OscDecode /strip/mute [1, 0.0]
// OscDecode /strip/solo [1, 1.0]
// OscDecode /strip/recenable [1, 1.0]

/// _stripUpdate(ssid, value, strips, propname) sends an event to the StripBloc
/// The master strip is handled with an ssid greater than the strip array length.
void _stripUpdate(int ssid, double value, List<Strip> strips, String propName) {
  var stripBloc = StripBloc();
  // debug sanity check, ssid can be -1 or zero if not set.
  assert(ssid > 0);
  var i = ssid - 1;
  var newStrip = strips[i];
  var newStrips = List<Strip>.from(strips);

  switch (propName) {
    // double
    case 'meter':
      newStrip = newStrip.copyWith(meter: value);
      break;
    case 'fader':
      newStrip = newStrip.copyWith(fader: value);
      break;
    case 'gain':
      newStrip = newStrip.copyWith(gain: value);
      break;
    // boolean
    case 'mute':
      newStrip = newStrip.copyWith(mute: value.toInt() == 1);
      break;
    case 'solo':
      newStrip = newStrip.copyWith(solo: value.toInt() == 1);
      break;
    case 'recenable':
      newStrip = newStrip.copyWith(recEnable: value.toInt() == 1);
      break;
    default:
      print('osc_feedback _stripUpdate unknown propName $propName');
      break;
  }

  newStrips[i] = newStrip;
  stripBloc.add(StripUpdatedEvent(newStrips));

  dPrint('stripUpdate /strip/$propName $value strip $newStrip');
}

/// OscDecode (msg): map message addresses to bloc events.
/// all the referenced blocs are singletons because this is outside of the widget tree.
// TODO: refactor TransportEvents to pass SendMsgType, ssid, value(s)
void OscDecode(OSCMessage msg) {
  var address = msg.address;
  var stripBloc = StripBloc();
  var settingsBloc = SettingsBloc();
  var transportBloc = TransportBloc();
  var transport = transportBloc.state.transport;

  dPrint('OscDecode $msg state ${transportBloc.state}');
  switch (address) {
    // mixer strip messages

    // setup mixer strips with these
    // OSCMessage(/reply, args: [AT, AudioMono, 1, 2, 0, 0, 1, 1])
    // OSCMessage(/reply, args: [AT, AudioStereo, 2, 2, 0, 0, 2, 1])
    // OSCMessage(/reply, args: [MT, MIDI, 0, 0, 2, 0, 0, 3])
    // OSCMessage(/reply, args: [B, AudioBus, 1, 2, 0, 0, 4])
    // OSCMessage(/reply, args: [MA, Master, 2, 2, 0, 0, 5])
    // OSCMessage(/reply, args: [end_route_list, 48000, 272884, 0])
    case '/reply':
      ReplySsid().increment();
      var strip = replyDecode(msg.arguments);
      stripBloc.add(StripAdd(strip));
      dPrint(
          'OscDecode /reply ${msg.arguments[0]} strip $strip ssid ${ReplySsid().ssid}');
      break;

    // meter messages
    // use the StripBloc to set the meter level.
    case '/master/meter':
      _stripUpdate(
          stripBloc.strips.length, msg.arguments[0], stripBloc.strips, 'meter');
      break;
    case '/strip/meter':
      _stripUpdate(
          msg.arguments[0], msg.arguments[1], stripBloc.strips, 'meter');
      break;
    // fader messages
    case '/master/fader':
      _stripUpdate(
          stripBloc.strips.length, msg.arguments[0], stripBloc.strips, 'fader');
      break;
    case '/strip/fader':
      _stripUpdate(
          msg.arguments[0], msg.arguments[1], stripBloc.strips, 'fader');
      break;
    // gain messages
    case '/master/gain':
      _stripUpdate(
          stripBloc.strips.length, msg.arguments[0], stripBloc.strips, 'gain');
      break;
    case '/strip/gain':
      _stripUpdate(
          msg.arguments[0], msg.arguments[1], stripBloc.strips, 'gain');
      break;
    // transport messages
    case '/heartbeat':
      // use the TransportBloc to blink the heartbeat LED.
      transportBloc.add(TransportReceivedEvent(transport.copyWith(
          heartbeat: (msg.arguments[0] as double).toInt() == 1)));
      break;
    // mute/solo/recenable use the Strip/Mixer blocs to set.
    case '/master/mute':
      _stripUpdate(
          stripBloc.strips.length, msg.arguments[0], stripBloc.strips, 'mute');
      break;
    case '/strip/mute':
      _stripUpdate(
          msg.arguments[0], msg.arguments[1], stripBloc.strips, 'mute');
      break;
    case '/strip/solo':
      _stripUpdate(
          msg.arguments[0], msg.arguments[1], stripBloc.strips, 'solo');
      break;
    case '/strip/recenable':
      _stripUpdate(
          msg.arguments[0], msg.arguments[1], stripBloc.strips, 'recenable');
      break;

    case '/position/time':
      // use the TransportBloc to set the currentTime string.
      transportBloc.add(TransportReceivedEvent(
          transport.copyWith(currentTime: msg.arguments[0])));
      dPrint('OscDecode /position/time ${msg.arguments[0]}');
      break;

    case '/rec_enable_toggle':
      // use the TransportBloc to set the isRecEnabled bool.
      transportBloc.add(TransportReceivedEvent(transport.copyWith(
          isRecEnabled: (msg.arguments[0] as double).toInt() == 1)));
      dPrint(
          'OscDecode /rec_enable_toggle ${msg.arguments[0]} ${transportBloc.state}');
      // also set the master strip
      _stripUpdate(stripBloc.strips.length, msg.arguments[0], stripBloc.strips,
          'recenable');
      break;

    case '/session_name':
      // use the SettingsBloc to set the session name.
      // print('OscDecode builder ${msg.arguments[0]}');
      settingsBloc.add(
        SettingsUpdatedEvent(
            settingsBloc.state.settings.copyWith(session: msg.arguments[0])),
      );
      break;

    case '/transport_play':
      // use the TransportBloc to set the isPlaying bool.
      var isPlaying = (msg.arguments[0] as double).toInt() == 1;
      transportBloc.add(
          TransportReceivedEvent(transport.copyWith(isPlaying: isPlaying)));
      dPrint(
          'OscDecode /transport_play ${msg.arguments[0]} isPlaying: $isPlaying ${transportBloc.state}');
      break;

    case '/transport_stop':
      // use the TransportBloc to set the isStopped bool.
      var isStopped = ((msg.arguments[0] as double).toInt() == 1) ||
          (!transport.isPlaying && !transport.isStopped);
      transportBloc.add(
          TransportReceivedEvent(transport.copyWith(isStopped: isStopped)));
      dPrint(
          'OscDecode /transport_stop $msg isStopped: $isStopped ${transportBloc.state}');
      break;

    default:
      dPrint('OscDecode unhandled $address ${msg.arguments}');
      break;
  }
}

/// replyDecode address, arguments converts the OSC message into a strip.
// Strip type - One of:
//   AT - Audio Track
//   MT - MIDI Track
//    B - Audio Bus
//   MB - MIDI bus
//   FB - Foldback bus
//    V - VCA
//   MA - Master
//   end_route_list - all done
Strip replyDecode(List<Object> arguments) {
  dPrint('StripBloc replyDecode arguments { $arguments }');
  Strip strip;
  // TODO: this could be a map.
  switch (arguments[0]) {
    case 'AT':
      strip = Strip.audio(arguments);
      break;
    case 'MT':
      strip = Strip.midi(arguments);
      break;
    case 'B':
      strip = Strip.audioBus(arguments);
      break;
    case 'MB':
      strip = Strip.midiBus(arguments);
      break;
    case 'FB':
      strip = Strip.foldbackBus(arguments);
      break;
    case 'V':
      strip = Strip.vca(arguments);
      break;
    case 'MA':
      strip = Strip.master(arguments);
      break;
    case 'end_route_list':
      strip = Strip.endRouteList(arguments);
      break;
    default:
      print('StripBloc replyDecode unknown track ${arguments[0]}');
      strip = null;
  }
  return strip;
}
