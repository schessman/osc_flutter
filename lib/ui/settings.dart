/// settings
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/blocs.dart';
import '../data/data.dart';
import '../ui/ui.dart';
import '../size_config.dart';

/// SettingsPage has state
class SettingsPage extends StatefulWidget {
  static const String routeName = '/settings';

  const SettingsPage({Key key}) : super(key: key);
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

/// SettingsPage sets IP and Port for Ardour OSC
class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, ss) {
        final settings = ss.settings ?? Settings();
        var addressController =
            TextEditingController(text: settings.address ?? '');
        var portController = TextEditingController(text: settings.port ?? '');

        return Scaffold(
          appBar: AppBar(
            leading: ArdourIcon(),
            title: TitleText(),
          ),
          drawer: navigationDrawer(),
          body: Center(
            child: SizedBox(
              width: SizeConfig.widthMultiplier * 80,
              child: Container(
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      TextField(
                        controller: addressController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          icon: IconButton(
                            icon: Icon(Icons.send),
                            tooltip: 'Accept address',
                            onPressed: () {
                              BlocProvider.of<SettingsBloc>(context).add(
                                SettingsUpdatedEvent(
                                  settings.copyWith(
                                      address: addressController.text),
                                ),
                              );
                            },
                          ),
                          hintText: settings.address,
                          helperText: 'Enter a host name or IP address',
                          counterText: '0 characters',
                          border: const OutlineInputBorder(),
                        ),
                        onSubmitted: (text) {
                          BlocProvider.of<SettingsBloc>(context).add(
                            SettingsUpdatedEvent(
                              settings.copyWith(address: text),
                            ),
                          );
                        },
                      ),
                      TextField(
                        controller: portController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          icon: IconButton(
                            icon: Icon(Icons.send),
                            tooltip: 'Accept port',
                            onPressed: () {
                              BlocProvider.of<SettingsBloc>(context).add(
                                SettingsUpdatedEvent(
                                  settings.copyWith(port: portController.text),
                                ),
                              );
                            },
                          ),
                          hintText: settings.port,
                          helperText: 'Enter a port number',
                          counterText: '0 characters',
                          border: const OutlineInputBorder(),
                        ),
                        onSubmitted: (text) {
                          BlocProvider.of<SettingsBloc>(context).add(
                            SettingsUpdatedEvent(
                              settings.copyWith(port: text),
                            ),
                          );
                        },
                      ),
                      Expanded(
                        child: FloatingActionButton(
                          child: const Icon(Icons.brightness_6),
                          onPressed: () => BlocProvider.of<ThemeBloc>(context)
                              .add(ThemeEvent.toggle),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
