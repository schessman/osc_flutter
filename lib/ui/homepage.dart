/// homepage - start here
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';

import '../ui/ui.dart';

/// HomePage has settings and mixer tabs
/// define the named route as routeName.
class HomePage extends StatefulWidget {
  static const String routeName = '/homepage';

  const HomePage({Key key}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: ArdourIcon(),
        title: TitleText(),
      ),
      drawer: navigationDrawer(),
      body: Navigator(
        key: _navigatorKey,
        initialRoute: '/',
        onGenerateRoute: (RouteSettings settings) {
          WidgetBuilder builder;
          switch (settings.name) {
            case '/':
              builder = (BuildContext context) => SplashPage();
              break;
            case '/settings':
              builder = (context) => SettingsPage();
              break;
            case '/mixer':
              builder = (context) => MixerPage();
              break;
            case '/transport':
              builder = (context) => TransportPage();
              break;
            case '/about':
              builder = (context) => AboutPage();
              break;
            default:
              throw Exception('Invalid route: ${settings.name}');
          }
          // You can also return a PageRouteBuilder and
          // define custom transitions between pages
          return MaterialPageRoute(
            builder: builder,
            settings: settings,
          );
        },
      ),
    );
  }
}
