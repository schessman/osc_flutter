/// mixer
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// import '../debug.dart';
import '../blocs/blocs.dart';
import '../size_config.dart';
import '../ui/ui.dart';
import '../widgets/mixer.dart';

/// MixerPage shows the mixers and meters.
/// Split in half, the left half contains the track widgets
/// and the right half has the master.  The space between is black container for now.
class MixerPage extends StatelessWidget {
  static const String routeName = '/mixer';

  const MixerPage({super.key});

  @override
  Widget build(BuildContext context) {
    // final _stripBloc = BlocProvider.of<StripBloc>(context);
    SizeConfig().init(context);

    return BlocBuilder<StripBloc, StripState>(
      // cubit: _stripBloc,
      builder: (BuildContext context, StripState ss) {
        return Scaffold(
          appBar: AppBar(
            leading: ArdourIcon(),
            title: TitleText(),
          ),
          drawer: navigationDrawer(),
          body: Container(
            margin: const EdgeInsets.all(5.0),
            color: Colors.black,
            height: SizeConfig.heightMultiplier * 95.0,
            child: Row(
              children: [
                // mixer strips other than master
                Flexible(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      // these need to get created dynamically based on /strip/list response
                      children: Mixers(context, ss),
                    ),
                  ),
                ),
                // space between
                SizedBox(
                  width: SizeConfig.widthMultiplier,
                ),
                // master strip
                Align(
                  alignment: Alignment.topRight,
                  child: Master(context, ss),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
