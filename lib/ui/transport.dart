/// transport
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/blocs.dart';
import '../osc/osc.dart';
import '../ui/ui.dart';
import '../size_config.dart';

/// TransportPage has state in bloc
/// TransportPage sets IP and Port for Ardour OSC
class TransportPage extends StatelessWidget {
  static const String routeName = '/transport';

  const TransportPage({super.key});

  Widget currentPosition(String currentTime) {
    return SizedBox(
      width: SizeConfig.widthMultiplier * 70.0,
      child: Center(
        child: Text(
          currentTime,
          style:
              TextStyle(height: 2, fontSize: SizeConfig.textMultiplier * 6.0),
        ),
      ),
    );
  }

  Widget transportButton(
      BuildContext context, IconData icon, color, whichButtonPressed) {
    return IconButton(
      icon: Icon(icon),
      iconSize: SizeConfig.imageSizeMultiplier * 8.0,
      color: color,
      onPressed: () {
        BlocProvider.of<IsolateBloc>(context)
            .add(IsolateSendEvent(whichButtonPressed));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // final _transportBloc = BlocProvider.of<TransportBloc>(context);
    SizeConfig().init(context);

    return BlocBuilder<TransportBloc, TransportState>(
      // cubit: _transportBloc,
      builder: (BuildContext context, ts) {
        return Scaffold(
          appBar: AppBar(
            leading: ArdourIcon(),
            title: TitleText(),
          ),
          drawer: navigationDrawer(),
          body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  currentPosition(ts.transport.currentTime),
                  Wrap(
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          transportButton(context, Icons.first_page,
                              Colors.cyan, SendMsgType.GotoStart),
                          transportButton(
                              context,
                              Icons.stop,
                              (ts.transport.isStopped
                                  ? Colors.red
                                  : Colors.cyan),
                              SendMsgType.Stop),
                          transportButton(
                              context,
                              Icons.play_arrow,
                              (ts.transport.isPlaying
                                  ? Colors.red
                                  : Colors.cyan),
                              SendMsgType.Play),
                          transportButton(context, Icons.last_page, Colors.cyan,
                              SendMsgType.GotoEnd),
                          transportButton(
                              context,
                              Icons.fiber_manual_record,
                              (ts.transport.isRecEnabled
                                  ? Colors.red
                                  : Colors.cyan),
                              SendMsgType.RecEnable),
                        ],
                      ),
                    ],
                  ),
                ]),
          ),
        );
      },
    );
  }
}
