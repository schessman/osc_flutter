/// about - describe the app
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';

import '../localization.dart';
import '../routes/routes.dart';
import '../size_config.dart';
import '../ui/ui.dart';

/// AboutPage gives  version, licence, git url, etc
class AboutPage extends StatelessWidget {
  static const String routeName = '/about';

  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      appBar: AppBar(
        leading: ArdourIcon(),
        title: TitleText(),
      ),
      drawer: navigationDrawer(),
      body: GestureDetector(
        onTap: () => Navigator.pushReplacementNamed(context, pageRoutes.home),
        child: Center(
          child: Container(
            width: SizeConfig.widthMultiplier * 90.0,
            height: SizeConfig.heightMultiplier * 90.0,
            margin: const EdgeInsets.all(5.0),
            color: Colors.black,
            child: SingleChildScrollView(
              child: Text(
                FlutterBlocLocalizations.instance.textArray('abouttext'),
                style: const TextStyle(fontSize: 24),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
