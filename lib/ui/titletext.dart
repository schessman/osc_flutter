/// titletext
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/blocs.dart';
import '../data/data.dart';

/// TitleText uses BloC to handle changed settings
class TitleText extends StatelessWidget {
  const TitleText({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, ss) {
        var settings = ss.settings ?? Settings();
        return Text(
            '${settings.appname}   ${settings.address}:${settings.port} "${settings.session}"');
      },
    );
  }
}
