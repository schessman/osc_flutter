/// navigation drawer
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';

// import '../debug.dart';
import '../localization.dart';
import '../routes/routes.dart';
import '../size_config.dart';

Widget createDrawerHeader() {
  return DrawerHeader(
    margin: EdgeInsets.zero,
    padding: EdgeInsets.zero,
    decoration: BoxDecoration(
        image: DecorationImage(
            fit: BoxFit.fitWidth,
            image: AssetImage('assets/img/Ardour-small-splash.png'))),
    child: null,
  );
}

Widget createDrawerBodyItem(
    {IconData icon, String text, GestureTapCallback onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text),
        )
      ],
    ),
    onTap: onTap,
  );
}

class navigationDrawer extends StatelessWidget {
  const navigationDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return SizedBox(
      width: SizeConfig.widthMultiplier * 35.0,
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Home',
              onTap: () =>
                  Navigator.pushReplacementNamed(context, pageRoutes.home),
            ),
            createDrawerBodyItem(
              icon: Icons.settings,
              text: 'Settings',
              onTap: () =>
                  Navigator.pushReplacementNamed(context, pageRoutes.settings),
            ),
            createDrawerBodyItem(
              icon: Icons.play_circle_filled,
              text: 'Transport',
              onTap: () =>
                  Navigator.pushReplacementNamed(context, pageRoutes.transport),
            ),
            createDrawerBodyItem(
              icon: Icons.mic,
              text: 'Mixer',
              onTap: () =>
                  Navigator.pushReplacementNamed(context, pageRoutes.mixer),
            ),
            Divider(),
            createDrawerBodyItem(
              icon: Icons.speaker_notes,
              text: FlutterBlocLocalizations.instance.text('about'),
              onTap: () =>
                  Navigator.pushReplacementNamed(context, pageRoutes.about),
            ),
            ListTile(
              title: Text('App version 1.0.0'),
              onTap: () =>
                  Navigator.pushReplacementNamed(context, pageRoutes.home),
            ),
          ],
        ),
      ),
    );
  }
}
