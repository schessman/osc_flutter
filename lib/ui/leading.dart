/// leading
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/blocs.dart';

/// ArdourIcon is for the heartbeat, and when pressed shows about or licence depending on state.
/// It animates from transparent to opaque, and enlarges at start of transition.
// TODO: handle tap on Image, with ts.transport.heartbeat selecting about or licence drawer.
class ArdourIcon extends StatelessWidget {
  const ArdourIcon({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransportBloc, TransportState>(builder: (context, ts) {
      return GestureDetector(
        onTap: () {
          Scaffold.of(context).openDrawer();
        },
        child: AnimatedOpacity(
          // If the widget is visible, animate to 0.3 (dimly visible).
          // If the widget is hidden, animate to 1.0 (fully visible).
          opacity: ts.transport.heartbeat ? 1.0 : 0.3,
          duration: Duration(milliseconds: 900),
          // The widget must be a child of the AnimatedOpacity widget.
          child: Container(
            padding: EdgeInsets.all(7.0),
            child: Image.asset('assets/img/ardour.png'),
          ),
        ),
      );
    });
  }
}
