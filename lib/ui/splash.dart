/// splash
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/blocs.dart';

/// SplashPage displays the Ardour splash image.
/// TODO: check to see if room for large image, otherwise use small image.
/// It animates from transparent to opaque if connected to Ardour.
/// The Scaffold Drawer is opened if the image is tapped.
class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransportBloc, TransportState>(builder: (context, ts) {
      return GestureDetector(
        onTap: () {
          Scaffold.of(context).openDrawer();
        },
        child: AnimatedOpacity(
          // If the widget is visible, animate to 0.3 (dimly visible).
          // If the widget is hidden, animate to 1.0 (fully visible).
          opacity: ts.transport.heartbeat ? 1.0 : 0.3,
          duration: Duration(milliseconds: 900),
          // The widget must be a child of the AnimatedOpacity widget.
          child: Container(
            padding: EdgeInsets.all(7.0),
            child: Image.asset('assets/img/Ardour-splash.png'),
          ),
        ),
      );
    });
  }
}
