/// osc_flutter routes
library;

// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:
//
import '../ui/ui.dart';

/// page routes.
class pageRoutes {
  static const String home = HomePage.routeName;
  static const String settings = SettingsPage.routeName;
  static const String mixer = MixerPage.routeName;
  static const String transport = TransportPage.routeName;
  static const String about = AboutPage.routeName;
}
