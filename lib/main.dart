// The original content is temporarily commented out to allow generating a self-contained demo - feel free to uncomment later.

// /// osc_flutter main - App using OSC to manage Ardour6 on remote system.
// // Copyright (C) 2020 Samuel Chessman
// // SPDX-License-Identifier: GPL-3.0-or-later
// // vim: ts=2 sw=2 noai:
// //
// // uses the bloc 5.0.0 package to handle state
// // see https://bloclibrary.dev/#/recipesflutterblocaccess?id=local-access
//
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';
//
// // for debugPaintSizeEnabled=true;
// import 'package:flutter/rendering.dart';
//
// import './localization.dart';
// import './ui/ui.dart';
// import './blocs/blocs.dart';
// import './routes/routes.dart';
//
// /// main: Flutter entry point. Setup BlocObserver, Blocs and the Widget tree.
// void main() {
//   debugPaintSizeEnabled = true;
//   Bloc.observer = SimpleBlocObserver();
//
//   // run the app with all the blocs provided for.
//   runApp(BlocSetup(OscApp()));
// }
//
// /// OscApp: Setup Widget tree with Material App and theme Bloc.
// class OscApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<IsolateBloc, IsolateState>(
//       builder: (context, message) {
//         return BlocBuilder<ThemeBloc, ThemeData>(
//           builder: (context, theme) {
//             return MaterialApp(
//               onGenerateTitle: (BuildContext context) =>
//                   FlutterBlocLocalizations.instance.text('title'),
//               localizationsDelegates: [
//                 const FlutterBlocLocalizationsDelegate(),
//                 GlobalMaterialLocalizations.delegate,
//                 GlobalWidgetsLocalizations.delegate,
//               ],
//               /* default english, I'm a native speaker. */
//               supportedLocales: [
//                 const Locale('en', ''),
//                 const Locale('de', ''),
//                 const Locale('es', ''),
//                 const Locale('fr', ''),
//                 const Locale('it', ''),
//                 const Locale('pt', ''),
//               ],
//               localeResolutionCallback:
//                   (Locale locale, Iterable<Locale> supportedLocales) {
//                 for (var supportedLocale in supportedLocales) {
//                   if (supportedLocale.languageCode == locale.languageCode ||
//                       supportedLocale.countryCode == locale.countryCode) {
//                     return supportedLocale;
//                   }
//                 }
//                 return supportedLocales.first;
//               },
//               theme: theme,
//               debugShowCheckedModeBanner: false,
//               home: HomePage(),
//               routes: {
//                 pageRoutes.home: (context) => HomePage(),
//                 pageRoutes.settings: (context) => SettingsPage(),
//                 pageRoutes.mixer: (context) => MixerPage(),
//                 pageRoutes.transport: (context) => TransportPage(),
//                 pageRoutes.about: (context) => AboutPage(),
//               },
//             );
//           },
//         );
//       },
//     );
//   }
// }
//

import 'package:flutter/material.dart';
import 'package:osc_flutter/src/rust/api/simple.dart';
import 'package:osc_flutter/src/rust/frb_generated.dart';

Future<void> main() async {
  await RustLib.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text('flutter_rust_bridge quickstart')),
        body: Center(
          child: Text(
              'Action: Call Rust `greet("Tom")`\nResult: `${greet(name: "Tom")}`'),
        ),
      ),
    );
  }
}
