/// mixer Widgets Mixers, Master, MixerStrip
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../debug.dart';
import '../blocs/blocs.dart';
import '../osc/osc.dart';
import '../size_config.dart';
import '../widgets/led.dart';
import '../widgets/slider.dart';

/// EdgeInsets const for standard insets
const _inset = EdgeInsets.all(1);
const _stripWidth = 16.0;

/// get a list of Widgets for the mixer from the current state.
List<Widget> Mixers(BuildContext context, StripState state) {
  dPrint('mixers state { $state } strips { ${state.strips} }');

  return state.strips.isEmpty
      ? <Widget>[]
      : state.strips
          .getRange(0, state.strips.length - 1)
          .map((strip) => MixerStrip(context, strip))
          .toList();
  // return state.strips.where((s) => s.type != 'MA').toList().map((strip) => MixerStrip(context, strip)).toList();
}

/// convert the master strip into a widget.
Widget Master(BuildContext context, StripState state) {
  dPrint('master state { $state } strips { ${state.strips} }');
  return state.strips.isEmpty
      ? Container()
      : MixerStrip(context, state.strips[state.strips.length - 1]);
}

/// display a vertical slider to control volume.
Widget _fader(BuildContext context, Strip strip) {
  // assume portrait
  var width = SizeConfig.widthMultiplier * 7.0;
  var height = SizeConfig.heightMultiplier * 62.0;
  var trackBarHeight = SizeConfig.heightMultiplier * 4.0;
  if (!SizeConfig.isPortrait) {
    width = SizeConfig.widthMultiplier * 5.0;
    height = SizeConfig.heightMultiplier * 55.0;
  }
  return Container(
    width: width,
    height: height,
    margin: _inset,
    color: Colors.grey,
    child: FlutterSlider(
      values: [strip.fader],
      max: 1.0,
      min: 0.0,
      step: FlutterSliderStep(step: 0.005),
      rtl: true,
      axis: Axis.vertical,
      selectByTap: true, // default is true
      onDragging: (handlerIndex, lowerValue, upperValue) {
        // First value is handlerIndex, which determines the handler.
        // 0 is Left Handler and 1 refers to Right Handler
        dPrint(
            'flutterSlider onDragging ssid $strip.ssid index $handlerIndex, value ${lowerValue.runtimeType} $lowerValue other $upperValue');
        // figure out which strip is dragging, send OSC message to Ardour.
        // handle master strip with different message.
        BlocProvider.of<IsolateBloc>(context).add(IsolateSendEvent(
            strip.type == 'MA' ? SendMsgType.MasterFader : SendMsgType.Fader,
            ssid: strip.ssid,
            value: lowerValue));
      },
      trackBar: FlutterSliderTrackBar(
        inactiveTrackBar: BoxDecoration(
          borderRadius: BorderRadius.circular(SizeConfig.widthMultiplier * 0.3),
          color: Colors.black,
          border: Border.all(
              width: SizeConfig.widthMultiplier * 0.3,
              color: Colors.blueGrey[800]),
        ),
        activeTrackBar: BoxDecoration(
            borderRadius:
                BorderRadius.circular(SizeConfig.widthMultiplier * 0.2),
            color: Colors.grey[400].withOpacity(0.5)),
        activeTrackBarHeight: trackBarHeight,
        inactiveTrackBarHeight: trackBarHeight,
      ),
      handler: FlutterSliderHandler(
          decoration: BoxDecoration(),
          child: Container(
            decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.circular(SizeConfig.widthMultiplier * 0.2),
                color: Colors.grey[800],
                border: Border.all(
                    color: Colors.black.withOpacity(0.65),
                    width: SizeConfig.widthMultiplier * 0.2)),
          )),
      rightHandler: FlutterSliderHandler(
          decoration: BoxDecoration(),
          child: Container(
            decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.circular(SizeConfig.widthMultiplier * 0.3),
                color: Colors.grey[800],
                border: Border.all(
                    color: Colors.black.withOpacity(0.65),
                    width: SizeConfig.widthMultiplier * 0.2)),
          )),
      handlerWidth: SizeConfig.widthMultiplier * 4.0,
      handlerHeight: SizeConfig.heightMultiplier * 2.0,
    ),
  );
}

/// display a vertical volume meter.
Widget _meter(double meter) {
  // assume portrait
  var width = SizeConfig.widthMultiplier * 7.0;
  var height = SizeConfig.heightMultiplier * 62.0;
  if (!SizeConfig.isPortrait) {
    width = SizeConfig.widthMultiplier * 5.0;
    height = SizeConfig.heightMultiplier * 55.0;
  }
  return Container(
    width: width,
    height: height,
    margin: _inset,
    color: Colors.green,
    // this widget does all the work.
    child: Led(
      currentValue: (meter * 100.0).round(),
      maxValue: 100,
      // minValue: 0,
      size: 40,
      // animatedDuration: const Duration(milliseconds: 400),
      animatedDuration: const Duration(milliseconds: 40),
      direction: Axis.vertical,
      verticalDirection: VerticalDirection.up,
      borderRadius: 1,
      border: Border.all(
        color: Colors.indigo,
        width: SizeConfig.widthMultiplier * 0.2,
      ),
      backgroundColor: Colors.blueGrey[800],
      progressColor: Colors.lightGreenAccent[400],
      changeColorValue: 1,
      changeProgressColor: Colors.amber[700],
    ),
  );
}

/// _valueBox(val): display a value in a box with one decimal.
Widget _valueBox(double val) {
  return Container(
    width: SizeConfig.widthMultiplier * 6.0,
    height: SizeConfig.heightMultiplier * 4.0,
    margin: _inset,
    color: Colors.black,
    child: Center(
      child: Text(val.toStringAsFixed(1)),
    ),
  );
}

/// _resetPeak(context, strip) - set peak to -Infinity
void _resetPeak(Strip strip) {
  var stripBloc = StripBloc();
  var newStrip = strip.copyWith(peak: double.negativeInfinity);
  var newStrips = List<Strip>.from(stripBloc.state.strips);
  newStrips[strip.ssid - 1] = newStrip;
  stripBloc.add(StripUpdatedEvent(newStrips));
}

/// _faderMeter(context, strip): Widget that has vertical volume fader and led strip meter.
/// made up of two vertical widgets each with a text box above.
// TODO: allow input from gain box.
Widget _faderMeter(BuildContext context, Strip strip) {
  return Row(
    children: <Widget>[
      Column(
        children: <Widget>[
          _valueBox(strip.gain),
          _fader(context, strip),
        ],
      ),
      Container(
        width: SizeConfig.widthMultiplier * 0.5,
        // height: SizeConfig.heightMultiplier * 2.0,
      ),
      Column(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              _resetPeak(strip);
            },
            child: _valueBox(strip.peak),
          ),
          _meter(strip.meter),
        ],
      ),
    ],
  );
}

/// _faderMeterBoxes(context, strip): Widget that has button boxes, vertical volume fader and led strip meter.
/// made up of three columns, one of buttons, two vertical widgets each with a text box above.
/// only in landscape mode
Widget _faderMeterBoxes(BuildContext context, Strip strip) {
  return Row(
    children: <Widget>[
      Column(
        children: <Widget>[
          _recEnableBox(context, strip),
          // mute/solo
          _muteSoloBox(context, strip),
        ],
      ),
      // Container(
      //   width: SizeConfig.widthMultiplier * 0.5,
      // height: SizeConfig.heightMultiplier * 2.0,
      // ),
      Column(
        children: <Widget>[
          _valueBox(strip.gain),
          _fader(context, strip),
        ],
      ),
      Container(
        width: SizeConfig.widthMultiplier * 0.5,
        // height: SizeConfig.heightMultiplier * 2.0,
      ),
      Column(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              _resetPeak(strip);
            },
            child: _valueBox(strip.peak),
          ),
          _meter(strip.meter),
        ],
      ),
    ],
  );
}

/// Display the track name in a box.
Widget _nameBox(String name) {
  // assume portrait
  var width = SizeConfig.widthMultiplier * _stripWidth;
  var height = SizeConfig.heightMultiplier * 3.0;
  if (!SizeConfig.isPortrait) {
    width = SizeConfig.widthMultiplier * 28.0;
    height = SizeConfig.heightMultiplier * 5.0;
  }
  return Container(
    width: width,
    height: height,
    margin: _inset,
    color: Colors.black,
    child: Center(
      child: Text(name),
    ),
  );
}

/// button common widget
Widget _button(BuildContext context, Strip strip, double size, Color color,
    int type, dynamic value) {
  return IconButton(
    icon: Icon(Icons.fiber_manual_record),
    iconSize: size,
    padding: _inset,
    constraints: BoxConstraints.expand(width: size + 1.0, height: size + 1.0),
    color: color,
    onPressed: () {
      BlocProvider.of<IsolateBloc>(context).add(
        IsolateSendEvent(type, ssid: strip.ssid, value: value),
      );
    },
  );
}

/// Display the recEnable button in a box.
/// handle the press to toggle strip.
Widget _recEnableBox(BuildContext context, Strip strip) {
  // assume portrait
  var width = SizeConfig.widthMultiplier * _stripWidth;
  var height = SizeConfig.heightMultiplier * 8.0;
  var iconSize = SizeConfig.imageSizeMultiplier * 8.0;
  if (!SizeConfig.isPortrait) {
    width = SizeConfig.widthMultiplier * 8.0;
    height = SizeConfig.heightMultiplier * 16.0;
    iconSize = SizeConfig.imageSizeMultiplier * 8.0;
  }
  if (strip.type == 'B' || strip.type == 'MB') {
    // Empty space filler for strips without record enable
    return SizedBox(
      width: width,
      height: height * 1.03,
    );
  }
  return Container(
    width: width,
    height: height,
    margin: _inset,
    color: Colors.black,
    child: Center(
      child: _button(
        context,
        strip,
        iconSize,
        strip.recEnable ? Colors.red : Colors.cyan,
        strip.type == 'MA' ? SendMsgType.RecEnable : SendMsgType.StripRecEnable,
        !strip.recEnable,
      ),
    ),
  );
}

/// Display the mute and Solo buttons in a box.
/// handle presses to toggle.
// TODO: factor out button common code.
Widget _muteSoloBox(BuildContext context, Strip strip) {
  // assume portrait
  var width = SizeConfig.widthMultiplier * _stripWidth;
  var widthOne = SizeConfig.widthMultiplier * 5.5;
  var height = SizeConfig.heightMultiplier * 5.0;
  var iconSize = SizeConfig.imageSizeMultiplier * 5.0;
  if (!SizeConfig.isPortrait) {
    width = SizeConfig.widthMultiplier * 8.0;
    widthOne = SizeConfig.widthMultiplier * 8.0;
    height = SizeConfig.heightMultiplier * 32.0;
    iconSize = SizeConfig.imageSizeMultiplier * 8.0;

    return Container(
      width: width,
      height: height,
      // margin: _inset,
      color: Colors.black,
      child: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              width: widthOne,
              height: iconSize,
              child: _button(
                context,
                strip,
                iconSize,
                strip.mute ? Colors.yellow : Colors.cyan,
                strip.type == 'MA' ? SendMsgType.MasterMute : SendMsgType.Mute,
                !strip.mute,
              ),
            ),
            SizedBox(
              width: widthOne,
              height: iconSize,
              child: (strip.type == 'MA')
                  ? Container()
                  : _button(
                      context,
                      strip,
                      iconSize,
                      strip.solo ? Colors.lightGreen[500] : Colors.cyan,
                      SendMsgType.Solo,
                      !strip.solo,
                    ),
            ),
          ],
        ),
      ),
    );
  }
  // portrait
  return Container(
    width: width,
    height: height,
    // margin: _inset,
    color: Colors.black,
    child: Center(
      child: Row(
        children: <Widget>[
          SizedBox(
            width: widthOne,
            child: _button(
              context,
              strip,
              iconSize,
              strip.mute ? Colors.yellow : Colors.cyan,
              strip.type == 'MA' ? SendMsgType.MasterMute : SendMsgType.Mute,
              !strip.mute,
            ),
          ),
          SizedBox(
            width: widthOne,
            child: (strip.type == 'MA')
                ? Container()
                : _button(
                    context,
                    strip,
                    iconSize,
                    strip.solo ? Colors.lightGreen[500] : Colors.cyan,
                    SendMsgType.Solo,
                    !strip.solo,
                  ),
          ),
        ],
      ),
    ),
  );
}

/// Display the track comments in a box
Widget _commentBox(comments) {
  // assume portrait
  var width = SizeConfig.widthMultiplier * _stripWidth;
  var height = SizeConfig.heightMultiplier * 4.0;
  if (!SizeConfig.isPortrait) {
    width = SizeConfig.widthMultiplier * 22.0;
    height = SizeConfig.heightMultiplier * 4.0;
  }
  return Container(
    width: width,
    height: height,
    margin: _inset,
    color: Colors.blue,
    child: Center(
      child: Text(comments),
    ),
  );
}

/// MixerStrip converts Strips into widgets.  The Strip type is used to select the layout.
// Audio and Midi Strips have:
// Track Name (maybe group color)
// Processor Box (not yet)
// Panner
// Recording Options (Rec Enable/In/Disk)
// (Iso/Lock) Mute/Solo
// Gain text/Peak level Gain slider/LED Meter max +6
// comments

// TODO: vary according to landscape/portrait.
//
// Audio and Midi Strips have:
// Track Name (maybe group color)
// Processor Box (not yet)
// Panner
// Recording Options (Rec Enable/In/Disk)
// (Iso/Lock) Mute/Solo
// Gain text/Peak level Gain slider/LED Meter max +6
// comments
//
// AudioBusStrip has no recenable.
// MidiBusStrip has no recenable.
// Audio and Midi Buses have: no Recording Options
// Track Name (maybe group color)
// Processor Box (not yet)
// Panner
// Aux (not enabled)
// (Iso/Lock) Mute/Solo
// Gain text/Peak level Gain slider/LED Meter
// comments
//
// VCA has multiple columns per number/name.
// Mute/Solo
// Gain
//
// Foldback has multiple send control blocks and can be mono or stereo
// Foldback Name
// Send Control Block(s)
//   send name, active LED | Pan Azimuth knob for Stereo only
//   horizontal level slider
// Panner
// Processor Box (not yet)
// Foldback Level knob
// comments
//
// Master Strip has:
// No Color, No RecEnable, No Solo/Iso
// Track Name - Master
// Processor Box (not yet)
// Panner
// Mute
// Gain text/Peak level Gain slider/LED Meter max +20
// comments
//
// So methods can be called depending on strip type
// track name and color: Audio, Midi, Audio Bus, Midi Bus, VCA
// Processor Box: Audio, Midi, Audio Bus, Midi Bus, Master, Foldback
// Panner: Audio, Midi, Audio Bus, Midi Bus
// Recording Options (Rec Enable/In/Disk): Audio, Midi
// Iso/Lock: Audio, Midi, Audio Bus, Midi Bus
// Mute/Solo: Audio, Midi, Audio Bus, Midi Bus, VCA
// Mute only: Master
// Gain text/Peak level above Gain slider/LED Meter:  Audio, Midi, Audio Bus, Midi Bus, Master
// comments
//
// vca and foldback are not compatible with this abstraction so can't use MixerStrip.

Widget MixerStrip(BuildContext context, Strip strip) {
  dPrint('MixerStrip $strip');
  // assume portrait
  var width = SizeConfig.widthMultiplier * _stripWidth;
  var height = SizeConfig.heightMultiplier * 93.0;
  if (SizeConfig.isPortrait) {
    return Container(
      width: width,
      height: height,
      margin: _inset,
      color: Colors.black,
      child: Column(
        children: <Widget>[
          // Name
          _nameBox(strip.name),
          // Pan
          // Recording Options
          _recEnableBox(context, strip),
          // mute/solo
          _muteSoloBox(context, strip),
          // fader/meter
          _faderMeter(context, strip),
          // comments
          _commentBox(strip.comments),
        ],
      ),
    );
  } else {
    // landscape
    width = SizeConfig.widthMultiplier * 24.0;
    height = SizeConfig.heightMultiplier * 69.0;
    return Container(
      width: width,
      height: height,
      margin: _inset,
      color: Colors.black,
      child: Column(
        children: <Widget>[
          // Name
          _nameBox(strip.name),
          // fader/meter
          _faderMeterBoxes(context, strip),
          // comments
          _commentBox(strip.comments),
        ],
      ),
    );
  }
}
