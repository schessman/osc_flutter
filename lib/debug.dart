/// debug - osc_flutter debug printing.
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:
//
// uses the bloc 5.0.0 package to handle state
// see https://bloclibrary.dev/#/recipesflutterblocaccess?id=local-access

// TODO: more levels of print than on/off

const t = String.fromEnvironment('DEBUGPRINT');
bool debugprint = (t == '1' ? true : false);

void dPrint(s) {
  if (debugprint) print(s);
}
