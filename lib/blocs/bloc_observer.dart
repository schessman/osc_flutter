/// bloc_observer: watch events, transitions and errors
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'package:flutter_bloc/flutter_bloc.dart';

import '../debug.dart';

/// SimpleBlocObserver: monitor bloc events, transitions, errors, changes.
class SimpleBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    dPrint('onEvent ${bloc.state.hashCode}');
    dPrint(event);
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    dPrint('onTransition ${bloc.state.hashCode}');
    dPrint(transition);
    super.onTransition(bloc, transition);
  }

  @override
  void onError(Cubit cubit, Object error, StackTrace stackTrace) {
    dPrint('onError ${cubit.state.hashCode}');
    dPrint(error);
    super.onError(cubit, error, stackTrace);
  }

  @override
  void onChange(Cubit cubit, Change change) {
    dPrint('onChange ${cubit.state.hashCode}');
    dPrint('${cubit.runtimeType} $change');
    super.onChange(cubit, change);
  }
}
