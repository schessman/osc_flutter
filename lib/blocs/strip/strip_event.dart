/// strip_event
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'strip_bloc.dart';

abstract class StripEvent extends Equatable {
  const StripEvent();

  @override
  List<Object> get props => [];
}

class StripInitialize extends StripEvent {}

class StripAdd extends StripEvent {
  final Strip strip;

  const StripAdd(this.strip);

  @override
  List<Object> get props => [strip];

  @override
  String toString() => 'StripAdd event $strip';
}

class StripDelete extends StripEvent {
  final Strip strip;

  const StripDelete(this.strip);

  @override
  List<Object> get props => [strip];

  @override
  String toString() => 'StripDelete event $strip';
}

class StripUpdatedEvent extends StripEvent {
  final List<Strip> strips;

  const StripUpdatedEvent(this.strips);

  @override
  List<Object> get props => [strips];

  @override
  String toString() => 'StripUpdatedEvent event $strips';
}
