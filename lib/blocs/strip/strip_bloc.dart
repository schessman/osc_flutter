/// strip_bloc
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'dart:async';
import 'dart:math'; // for peak dB calculation in Strip
import 'package:equatable/equatable.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';

import '../../debug.dart';

part 'strip.dart';
part 'strip_event.dart';
part 'strip_state.dart';

/// StripBloc manages the list of strips, adds/deletes/updates strips.
/// Strip UI events don't use StripBloc, go directly to Isolate.
/// StripBloc sends MixerUpdatedEvent when end_route_list strip reply is received.
/// StripBloc needs to be singleton to be used outside of widget tree.
class StripBloc extends Bloc<StripEvent, StripState> {
  static final StripBloc _singleton = StripBloc._internal();

  factory StripBloc() => _singleton;
  StripBloc._internal() : super(StripUpdatedState(<Strip>[]));

  // used by settings_bloc
  List<Strip> get strips => state.strips;

  /// StripBloc business logic.  Does not change state on StripUpdatedEvent.
  @override
  Stream<StripState> mapEventToState(StripEvent event) async* {
    dPrint('StripBloc mapEvent state { $state } event { $event }');
    if (event is StripInitialize) {
      yield* _mapStripInitializeToState();
    } else if (event is StripAdd) {
      yield* _mapStripAddToState(event);
    } else if (event is StripDelete) {
      yield* _mapStripDeleteToState(event);
    } else if (event is StripUpdatedEvent) {
      yield* _mapStripUpdatedEventToState(event);
    } else {
      // wtf
      print('StripBloc mapEvent unknown event { $event }');
    }
  }

  /// _mapStripInitializeToState initializes the state to empty list of Strip.
  Stream<StripState> _mapStripInitializeToState() async* {
    dPrint('StripBloc _mapStripInitializeToState state ${state.hashCode}');
    yield StripUpdatedState(<Strip>[]);
  }

  /// _mapStripAddToState adds the strip to the state list.
  Stream<StripState> _mapStripAddToState(StripAdd event) async* {
    if (state is StripInitialState) {
      dPrint(
          'StripBloc _mapStripAddToState Initial state ${state.hashCode} event { $event }');
    } else if (state is StripUpdatedState) {
      final strips = List<Strip>.from((state as StripUpdatedState).strips);
      dPrint(
          'StripBloc _mapStripAddToState Updated state ${state.hashCode} event { $event } _strips { $strips }');

      // probably could use dart null aware operators better.
      // let the Mixer Bloc know the strips have changed if it is the end_route_list strip.
      // Send it a list of Strips with master in the strips.
      if (event.strip is StripEndRouteList) {
        dPrint(
            'StripBloc _mapStripAddToState add StripEndRouteList _strips { $strips }');
        add(StripUpdatedEvent(strips));
      } else {
        strips.add(event.strip);
        dPrint(
            'StripBloc _mapStripAddToState strips after add event { $event } _strips { $strips }');
      }
      yield StripUpdatedState(strips);
    }
  }

  /// _mapStripDeleteToState removes the event strip from the state list.
  Stream<StripState> _mapStripDeleteToState(StripDelete event) async* {
    dPrint('StripBloc _mapStripDeleteToState event { $event }');
    if (state is StripUpdatedState) {
      // Remove a single item.
      final strips = (state as StripUpdatedState)
          .strips
          .where((strip) => strip.key != event.strip.key)
          .toList();
      yield StripUpdatedState(strips);
      // handle the Strips. Break into parts for debug print.
      // var _stripsOnly = _strips.where((s) => s.type != 'MA').toList();
      add(StripUpdatedEvent(strips));
      dPrint('StripBloc _mapStripDeleteToState _strips { $strips }');
    }
  }

  /// _mapStripUpdatedEventToState sends MixerUpdatedEvent with current state to MixerBloc.
  Stream<StripState> _mapStripUpdatedEventToState(
      StripUpdatedEvent event) async* {
    dPrint('StripBloc _mapStripUpdatedEventToState event { $event }');
    yield StripUpdatedState(event.strips);
  }
}
