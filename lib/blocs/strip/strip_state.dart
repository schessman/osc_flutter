/// strip_state
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'strip_bloc.dart';

/// StripState abstract class for StripInitialState and StripUpdatedState
/// Instantiated states use Strip class to track state.
/// Can't be a singleton because multiple strips.
abstract class StripState extends Equatable {
  final List<Strip> strips;
  const StripState(this.strips);

  @override
  List<Object> get props => [strips];

  StripState copyWith({List<Strip> strips});
}

class StripInitialState extends StripState {
  const StripInitialState(super.strips);

  @override
  List<Object> get props => [strips];

  @override
  String toString() =>
      'StripInitialState $hashCode { strips: ${strips.hashCode} $strips }';

  // shallow copy, just a new StripState
  @override
  StripInitialState copyWith({List<Strip> strips}) {
    return StripInitialState(
      strips ?? this.strips,
    );
  }
}

class StripUpdatedState extends StripState {
  const StripUpdatedState(super.strips);

  @override
  List<Object> get props => [strips];

  @override
  String toString() =>
      'StripUpdatedState state $hashCode { strips: ${strips.hashCode} $strips }';

  // shallow copy, just a new StripState
  @override
  StripUpdatedState copyWith({List<Strip> strips}) {
    return StripUpdatedState(
      strips ?? this.strips,
    );
  }
}
