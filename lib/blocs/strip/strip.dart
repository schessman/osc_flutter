/// strip - the primary mixer element
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'strip_bloc.dart';

/// Strip abstract class because there are multiple types of strips (class StripType)
/// Audio, Midi, AudioBus, MidiBus, Vca, Master, Monitor, Foldback

/// Strip(type, {key}): Strip base class.
class Strip extends Equatable {
  // common props
  final String type;
  final GlobalKey key;
  final String name;
  final int ssid;
  final bool recEnable;
  final bool mute;
  final bool solo;
  final double gain;
  final double meter;
  final double fader;
  final double peak;
  final String comments;
  // master props
  final double trimdB;
  final double pan;
  // endroutelist props
  final int frameRate;
  final int frameNumber;
  final bool monitorPresent;

  Strip(this.type,
      {GlobalKey key,
      String name,
      int ssid,
      bool recEnable,
      bool mute,
      bool solo,
      double gain,
      double meter,
      double fader,
      double peak,
      String comments,
      // master props
      double trimdB,
      double pan,
      // endroutelist props
      int frameRate,
      int frameNumber,
      bool monitorPresent})
      : key = key ?? GlobalKey(),
        name = name,
        ssid = ssid,
        recEnable = recEnable,
        mute = mute,
        solo = solo,
        gain = gain,
        meter = meter,
        fader = fader,
        peak = peak,
        comments = comments,
        // master props
        trimdB = trimdB,
        pan = pan,
        // endroutelist props
        frameRate = frameRate,
        frameNumber = frameNumber,
        monitorPresent = monitorPresent;

  factory Strip.audio(arguments) => StripAudio.fromReply(arguments);
  factory Strip.master(arguments) => StripMaster.fromReply(arguments);
  factory Strip.midi(arguments) => StripMidi.fromReply(arguments);
  factory Strip.audioBus(arguments) => StripAudioBus.fromReply(arguments);
  factory Strip.midiBus(arguments) => StripMidiBus.fromReply(arguments);
  factory Strip.foldbackBus(arguments) => StripFoldbackBus.fromReply(arguments);
  factory Strip.vca(arguments) => StripInitial.fromReply(arguments);
  factory Strip.endRouteList(arguments) =>
      StripEndRouteList.fromReply(arguments);

  @override
  List<Object> get props => [
        type,
        key,
        name,
        ssid,
        recEnable,
        mute,
        solo,
        gain,
        meter,
        fader,
        peak,
        comments,
        // master props
        trimdB,
        pan,
        // endroutelist props
        frameRate,
        frameNumber,
        monitorPresent
      ];

  /// meterToPeak (meter, peak): capture the peak dB.
  /// handle a null peak with this.peak in caller
  /// meter can be null, use existing peak.
  double meterToPeak(double meter, double peak) {
    var p = peak;
    p = (meter * 100.0) - 94.0;
    if (p < peak) {
      p = peak;
    }
    if (p < -192.99) {
      p = double.negativeInfinity;
    }
    return p;
  }

  /// coefficientTodb(coeff): convert 0.0-1.0 to dB using the ardour algorithm.
  /// this is for the slider
  double coefficientTodB(double coeff) {
    if (coeff < 1e-15) {
      return double.negativeInfinity;
    }
    return ((20.0 * log(coeff)) / ln10);
  }

/*
 * c++ implementation from Ardour6
 *
  static inline float fast_coefficient_to_dB (float coeff) {
    return 20.0f * fast_log10 (coeff);
  }

  static inline float accurate_coefficient_to_dB (float coeff) {
    if (coeff < 1e-15) return  -std::numeric_limits<float>::infinity();
    return 20.0f * log10f (coeff);
  }
 */

  @override
  String toString() =>
      'Strip type $type, key $key, name $name, ssid $ssid, recEnable $recEnable, mute $mute, solo $solo, '
      'gain $gain, meter $meter, fader $fader, peak $peak, comments $comments'
      'trimdB $trimdB, pan $pan, frameRate $frameRate, frameNumber $frameNumber, monitorPresent $monitorPresent';

  Strip copyWith(
      {String name,
      int ssid,
      bool recEnable,
      bool mute,
      bool solo,
      double gain,
      double meter,
      double fader,
      double peak,
      String comments,
      // master props
      double trimdB,
      double pan,
      // endroutelist props
      int frameRate,
      int frameNumber,
      bool monitorPresent}) {
    return Strip(type,
        name: name ?? this.name,
        ssid: ssid ?? this.ssid,
        recEnable: recEnable ?? this.recEnable,
        mute: mute ?? this.mute,
        solo: solo ?? this.solo,
        gain: gain ?? this.gain,
        meter: meter ?? this.meter,
        fader: fader ?? this.fader,
        peak: meterToPeak(meter, peak ?? this.peak),
        comments: comments ?? this.comments,
        // master props
        trimdB: trimdB ?? this.trimdB,
        pan: pan ?? this.pan,
        // endroutelist props
        frameRate: frameRate ?? this.frameRate,
        frameNumber: frameNumber ?? this.frameNumber,
        monitorPresent: monitorPresent ?? this.monitorPresent);
  }
}

/// StripInitial() is a generic strip used to setup the bloc state.
class StripInitial extends Strip {
  StripInitial() : super('IN', name: 'StripInitial');

  StripInitial.fromReply(List<Object> arguments)
      : super('IN', name: arguments[0]);

  @override
  List<Object> get props => [type, key, name];

  @override
  String toString() => 'StripInitial type $type, key $key, name $name';
}

//
// The mixer strips breaks down into:

// Header:
// Track name
// Input(s): not displayed
// Polarity only for audio tracks: not displayed
// Processor box: not displayed
// Panner: not displayed
// Recording options: buttons
// Mute/Solo: buttons
// Gain & Meter: this is the heart of the strip
// Control master: not displayed
// Fader automation/mix group/metering point: not displayed
// Output(s): not displayed
// Comments: only for selected strip
//
// /strip/list asks Ardour for a list of strips that the current session has.
// Ardour replies with a message for each strip with the following information:
//
// 0 Strip type - One of:
//   AT - Audio Track
//   MT - MIDI Track
//   V - VCA
//   MA - Master
//   MO - Monitor
//   FB - Foldback bus
//   B - Audio Bus
//   MB - MIDI bus
// 1 Strip name
// 2 unknown
// 3 Number of inputs
// 4 Number of outputs
// 5 Muted
// 6 Soloed
// 7 SSID
// After all the strip messages have been sent, one final message is sent with:
//   The text end_route_list
//   The session frame rate
//   The last frame number of the session
//   Monitor section present
// OSCMessage(/reply, args: [AT, Audio  1, 0, 1, 2, 0, 0, 1])
// OSCMessage(/reply, args: [AT, Audio  2, 0, 1, 2, 0, 0, 2])
// OSCMessage(/reply, args: [MT, MIDI, 0, 0, 2, 1, 0, 3])
// OSCMessage(/reply, args: [B, Audio Bus, 1, 2, 0, 0, 4])

// TODO: refactor to have only one constructor.
/// StripEndRouteList(s):  maintains the frame rate, last frame number and monitor section boolean.
class StripEndRouteList extends Strip {
  // TODO: is this used?
  StripEndRouteList(List<String> s)
      : super('end_route_list',
            frameRate: int.parse(s[1] ?? '0'),
            frameNumber: int.parse(s[2] ?? '0'),
            monitorPresent: (s[3] == '1'));

  // TODO: make this the default constructor.
  StripEndRouteList.fromReply(List<Object> arguments)
      : super(
          'end_route_list',
          frameRate: arguments[1],
          frameNumber: arguments[2],
          monitorPresent: (arguments[3] == 1),
        );

  @override
  List<Object> get props => [type, key, frameRate, frameNumber, monitorPresent];

  @override
  String toString() =>
      'StripEndRouteList key $key, frameRate $frameRate, frameNumber $frameNumber, monitorPresent $monitorPresent';
}

// TODO: refactor to have only default constructor.
/// StripAudio(s): maintains the strip state for the Audio Track.
class StripAudio extends Strip {
  StripAudio(
      {name, ssid, recEnable, mute, solo, gain, meter, fader, peak, comments})
      : super(
          'AT',
          name: name ?? '',
          ssid: ssid ?? -1,
          mute: mute ?? false,
          solo: solo ?? false,
          recEnable: recEnable ?? false,
          gain: gain ?? 0.0,
          meter: meter ?? 0,
          fader: fader ?? 0.0,
          peak: peak ?? 0.0,
          comments: comments ?? '',
        );

  // 1 Strip name
  // 2 Number of inputs
  // 3 Number of outputs
  // 4 Muted
  // 5 Soloed
  // 6 SSID
  // 7 recEnable
  // OSCMessage(/reply, args: [AT, Audio      1, 0, 1, 2, 0, 1, 1])
  // OSCMessage(/reply, args: [AT, AudioMono, 1, 2, 0, 0, 1, 1])
  // OSCMessage(/reply, args: [AT, AudioStereo, 2, 2, 0, 0, 2, 1])
  // OSCMessage(/reply, args: [AT, Audio        2, 0, 1, 2, 0, 1, 2])
  StripAudio.fromReply(List<Object> arguments)
      : super(
          'AT',
          name: arguments[1],
          mute: arguments[4] == 1,
          solo: arguments[5] == 1,
          ssid: arguments[6],
          recEnable: arguments[7] == 1,
          gain: 0.0,
          peak: double.negativeInfinity,
          meter: 0.0,
          fader: 0.0,
          comments: '',
        );

  @override
  String toString() =>
      'StripAudio key $key, name $name, ssid $ssid, recEnable $recEnable, mute $mute, '
      'solo $solo, gain $gain, peak $peak, meter $meter, fader $fader, comments $comments';
}

/// StripMidi(s): maintains the strip state for the Midi Track.
/// for unknown reason the reply arguments are different.
// TODO: refactor to have only default constructor.
class StripMidi extends Strip {
  StripMidi(
      {name, ssid, recEnable, mute, solo, gain, meter, fader, peak, comments})
      : super(
          'MT',
          name: name ?? '',
          ssid: ssid ?? -1,
          mute: mute ?? false,
          solo: solo ?? false,
          recEnable: recEnable ?? false,
          gain: gain ?? 0.0,
          meter: meter ?? 0,
          fader: fader ?? 0.0,
          peak: peak ?? 0.0,
          comments: comments ?? '',
        );

  // 1 Strip name
  // 2 unknown
  // 3 Number of inputs
  // 4 Number of outputs
  // 5 Muted
  // 6 Soloed
  // 7 SSID
  // OSCMessage(/reply, args: [MT, MIDI, 0, 0, 2, 1, 0, 3])
  StripMidi.fromReply(List<Object> arguments)
      : super(
          'MT',
          name: arguments[1],
          mute: arguments[4] == 1,
          solo: arguments[5] == 1,
          ssid: arguments[6],
          recEnable: arguments[7] == 1,
          gain: 0.0,
          peak: double.negativeInfinity,
          meter: 0.0,
          fader: 0.0,
          comments: '',
        );

  @override
  String toString() =>
      'StripMidi key $key, name $name, ssid $ssid, recEnable $recEnable, mute $mute, '
      'solo $solo, gain $gain, peak $peak, meter $meter, fader $fader, comments $comments';
}

// Master OSC messages
// /master/gain dB where dB is a float ranging from -193 to +6 representing the actual gain of master in dB
// /master/fader position where position is an int ranging from 0 to 1023 representing the fader control position
// /master/trimdB dB where dB is a float ranging from -20 to +20 representing the actual trim for master in dB
// /master/pan_stereo_position position where position is a float ranging from 0 to 1 representing the actual pan position for master
// /master/mute state where state is a bool/int representing the actual mute state of the Master strip
// TODO: refactor to have only default constructor.
class StripMaster extends Strip {
  StripMaster(
      {name,
      ssid,
      mute,
      recEnable,
      gain,
      meter,
      peak,
      fader,
      trimdB,
      pan,
      comments})
      : super(
          'MA',
          name: name ?? 'Master',
          ssid: ssid ?? -1,
          mute: mute ?? false,
          recEnable: recEnable ?? false,
          gain: gain ?? 0.0,
          meter: meter ?? 0.0,
          peak: double.negativeInfinity,
          fader: fader ?? 0.0,
          comments: comments ?? '',
          trimdB: trimdB ?? 0.0,
          pan: pan ?? 0.0,
        );

  // 1 Strip name
  // 2 Number of inputs
  // 3 Number of outputs
  // 4 Muted
  // 5 ?
  // 6 SSID
  // [MA, Master, 2, 2, 0, 0, 5])
  StripMaster.fromReply(List<Object> arguments)
      : super(
          'MA',
          name: arguments[1],
          mute: arguments[4] == 1,
          recEnable: false,
          ssid: arguments[6],
          gain: 0.0,
          fader: 0.0,
          meter: 0.0,
          peak: double.negativeInfinity,
          comments: '',
          trimdB: 0.0,
          pan: 0.0,
        );

  @override
  List<Object> get props => [
        name,
        ssid,
        recEnable,
        gain,
        fader,
        trimdB,
        pan,
        mute,
        meter,
        peak,
        comments
      ];

  @override
  String toString() =>
      'StripMaster key $key, name $name, ssid $ssid, recEnable $recEnable, gain $gain, fader $fader, trimdB $trimdB, pan $pan, mute $mute, meter $meter peak $peak comments { $comments }';

/*
  @override
  StripMaster copyWith(
      {String name,
      int ssid,
      bool recEnable,
      bool mute,
      bool solo,
      double gain,
      double meter,
      double fader,
      double peak,
      String comments,
      // master props
      double trimdB,
      double pan,
      // endroutelist props
      int frameRate,
      int frameNumber,
      bool monitorPresent}) {
    return StripMaster(
      name: name ?? this.name,
      ssid: ssid ?? this.ssid,
      mute: mute ?? this.mute,
      gain: gain ?? this.gain,
      meter: meter ?? this.meter,
      peak: meterToPeak(meter, this.peak),
      fader: fader ?? this.fader,
      trimdB: trimdB ?? this.trimdB,
      pan: pan ?? this.pan,
      comments: comments ?? this.comments,
    );
  }
 */
}

/// StripAudioBus(s): maintains the strip state for the AudioBus Track.
/// the reply arguments are different.  No recEnable.
// TODO: refactor to have only default constructor.
class StripAudioBus extends Strip {
  StripAudioBus({name, ssid, mute, solo, gain, meter, fader, peak, comments})
      : super(
          'B',
          name: name ?? '',
          ssid: ssid ?? -1,
          mute: mute ?? false,
          solo: solo ?? false,
          gain: gain ?? 0.0,
          meter: meter ?? 0,
          fader: fader ?? 0.0,
          peak: peak ?? 0.0,
          comments: comments ?? '',
        );

  // 1 Strip name
  // 2 Number of inputs
  // 3 Number of outputs
  // 4 Muted
  // 5 Soloed
  // 6 SSID
  // OSCMessage(/reply, args: [B, Audio Bus, 1, 2, 0, 1, 4])
  StripAudioBus.fromReply(List<Object> arguments)
      : super(
          'B',
          name: arguments[1],
          mute: arguments[4] == 1,
          solo: arguments[5] == 1,
          ssid: arguments[6],
          gain: 0.0,
          peak: double.negativeInfinity,
          meter: 0.0,
          fader: 0.0,
          comments: '',
        );

  @override
  List<Object> get props =>
      [name, ssid, mute, solo, gain, peak, meter, fader, comments];

  @override
  String toString() =>
      'StripAudioBus key $key, name $name, ssid $ssid, mute $mute, '
      'solo $solo, gain $gain, peak $peak, meter $meter, fader $fader, comments $comments';

/*
  @override
  StripAudioBus copyWith(
      {String name,
      int ssid,
      bool recEnable,
      bool mute,
      bool solo,
      double gain,
      double meter,
      double fader,
      double peak,
      String comments,
      // master props
      double trimdB,
      double pan,
      // endroutelist props
      int frameRate,
      int frameNumber,
      bool monitorPresent}) {
    return StripAudioBus(
      name: name ?? this.name,
      ssid: ssid ?? this.ssid,
      mute: mute ?? this.mute,
      solo: solo ?? this.solo,
      gain: gain ?? this.gain,
      meter: meter ?? this.meter,
      fader: fader ?? this.fader,
      peak: meterToPeak(meter, this.peak),
      comments: comments ?? this.comments,
    );
  }
 */
}

/// StripMidiBus(s): maintains the strip state for the MidiBus Track.
/// The reply arguments are different.  No recEnable
// TODO: refactor to have only default constructor.
class StripMidiBus extends Strip {
  StripMidiBus({name, ssid, mute, solo, gain, meter, fader, peak, comments})
      : super(
          'MB',
          name: name ?? '',
          ssid: ssid ?? -1,
          mute: mute ?? false,
          solo: solo ?? false,
          gain: gain ?? 0.0,
          meter: meter ?? 0,
          fader: fader ?? 0.0,
          peak: peak ?? 0.0,
          comments: comments ?? '',
        );

  // 1 Strip name
  // 2 unknown
  // 3 Number of inputs
  // 4 Number of outputs
  // 5 Muted
  // 6 Soloed
  // 7 SSID
  // OSCMessage(/reply, args: [B, Audio Bus, 1, 2, 0, 1, 4])
  StripMidiBus.fromReply(List<Object> arguments)
      : super(
          'MB',
          name: arguments[1],
          mute: arguments[4] == 1,
          solo: arguments[5] == 1,
          ssid: arguments[6],
          gain: 0.0,
          peak: double.negativeInfinity,
          meter: 0.0,
          fader: 0.0,
          comments: '',
        );

  @override
  List<Object> get props =>
      [name, ssid, mute, solo, gain, peak, meter, fader, comments];

  @override
  String toString() =>
      'StripMidiBus key $key, name $name, ssid $ssid, mute $mute, '
      'solo $solo, gain $gain, peak $peak, meter $meter, fader $fader, comments $comments';

/*
  @override
  StripMidiBus copyWith(
      {String name,
      int ssid,
      bool recEnable,
      bool mute,
      bool solo,
      double gain,
      double meter,
      double fader,
      double peak,
      String comments,
      // master props
      double trimdB,
      double pan,
      // endroutelist props
      int frameRate,
      int frameNumber,
      bool monitorPresent}) {
    return StripMidiBus(
      name: name ?? this.name,
      ssid: ssid ?? this.ssid,
      mute: mute ?? this.mute,
      solo: solo ?? this.solo,
      gain: gain ?? this.gain,
      meter: meter ?? this.meter,
      fader: fader ?? this.fader,
      peak: meterToPeak(meter, this.peak),
      comments: comments ?? this.comments,
    );
  }
 */
}

/// StripFoldbackBus(s): maintains the strip state for the FoldbackBus Track.
/// The reply arguments are different.  No recEnable
// TODO: refactor to have only default constructor.
class StripFoldbackBus extends Strip {
  StripFoldbackBus({name, ssid, mute, solo, gain, meter, fader, peak, comments})
      : super(
          'FB',
          name: name ?? '',
          ssid: ssid ?? -1,
          mute: mute ?? false,
          solo: solo ?? false,
          gain: gain ?? 0.0,
          meter: meter ?? 0,
          fader: fader ?? 0.0,
          peak: peak ?? 0.0,
          comments: comments ?? '',
        );

  // 1 Strip name
  // 2 unknown
  // 3 Number of inputs
  // 4 Number of outputs
  // 5 Muted
  // 6 Soloed
  // 7 SSID
  StripFoldbackBus.fromReply(List<Object> arguments)
      : super(
          'FB',
          name: arguments[1],
          mute: arguments[4] == 1,
          solo: arguments[5] == 1,
          ssid: arguments[6],
          gain: 0.0,
          peak: double.negativeInfinity,
          meter: 0.0,
          fader: 0.0,
          comments: '',
        );

  @override
  List<Object> get props =>
      [name, ssid, mute, solo, gain, peak, meter, fader, comments];

  @override
  String toString() =>
      'StripFoldbackBus key $key, name $name, ssid $ssid, mute $mute, '
      'solo $solo, gain $gain, peak $peak, meter $meter, fader $fader, comments $comments';

/*
  @override
  StripFoldbackBus copyWith(
      {String name,
      int ssid,
      bool recEnable,
      bool mute,
      bool solo,
      double gain,
      double meter,
      double fader,
      double peak,
      String comments,
      // master props
      double trimdB,
      double pan,
      // endroutelist props
      int frameRate,
      int frameNumber,
      bool monitorPresent}) {
    return StripFoldbackBus(
      name: name ?? this.name,
      ssid: ssid ?? this.ssid,
      mute: mute ?? this.mute,
      solo: solo ?? this.solo,
      gain: gain ?? this.gain,
      meter: meter ?? this.meter,
      fader: fader ?? this.fader,
      peak: meterToPeak(meter, this.peak),
      comments: comments ?? this.comments,
    );
  }
 */
}

// /monitor/gain dB where dB is a float ranging from -193 to 6 representing the actual gain of monitor in dB
// /monitor/fader position where position is an int ranging from 0 to 1023 representing the fader control position
// /monitor/mute state where state is a bool/int representing the actual mute state of the Monitor strip
// /monitor/dim state where state is a bool/int representing the actual dim state of the Monitor strip
// /monitor/mono state where state is a bool/int representing the actual mono state of the Monitor strip
// TODO: copyWith
