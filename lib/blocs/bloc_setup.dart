/// bloc_setup: top level bloc provider
library;

// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

import './blocs.dart';

/// BlocSetup(app): Top level bloc providers.  app is the child Widget tree.
class BlocSetup extends StatelessWidget {
  final Widget app;
  const BlocSetup(this.app, {super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SettingsBloc>(
          create: (BuildContext context) => SettingsBloc()
            ..add(
                SettingsInitializeEvent()), // loads settings from shared preferences
        ),
        BlocProvider<IsolateBloc>(
          create: (BuildContext context) => IsolateBloc()
            ..add(IsolateInitialize()), // creates isolate for UDP I/O
        ),
        BlocProvider<ThemeBloc>(
          create: (BuildContext context) => ThemeBloc(),
        ),
        BlocProvider<StripBloc>(
          create: (BuildContext context) => StripBloc(),
        ),
        BlocProvider<TransportBloc>(
          create: (BuildContext context) => TransportBloc(),
        ),
      ],
      child: app,
    );
  }
}
