/// Transport_state - BLoC handling AppPage Transport states
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'transport_bloc.dart';

// TransportState
// The states we define:
// abstract state with transport values
// initial state: default transport values
// unknown: unknown transport values
// updated: any of the props have changed.
//

/// Transport is a singleton class that keeps the state of the Ardour transport.
class Transport extends Equatable {
  final bool isPlaying;
  final bool isStopped;
  final bool isRecEnabled;
  final String currentTime;
  final bool heartbeat;

  static final Transport _singleton = Transport._internal();

  const Transport._internal()
      : isPlaying = false,
        isStopped = true,
        isRecEnabled = false,
        currentTime = '00:00:00.000',
        heartbeat = false;

  factory Transport() => _singleton;

  const Transport.update(this.isPlaying, this.isStopped, this.isRecEnabled,
      this.currentTime, this.heartbeat);

  Transport copyWith(
      {bool isPlaying,
      bool isStopped,
      bool isRecEnabled,
      String currentTime,
      bool heartbeat}) {
    return Transport.update(
      isPlaying ?? this.isPlaying,
      isStopped ?? this.isStopped,
      isRecEnabled ?? this.isRecEnabled,
      currentTime ?? this.currentTime,
      heartbeat ?? this.heartbeat,
    );
  }

  @override
  List<Object> get props =>
      [isPlaying, isStopped, isRecEnabled, currentTime, heartbeat];

  @override
  String toString() =>
      'Transport:$hashCode { isPlaying: $isPlaying, isStopped: $isStopped, isRecEnabled: $isRecEnabled, currentTime: $currentTime , heartbeat: $heartbeat }';
}

abstract class TransportState extends Equatable {
  final Transport transport;
  const TransportState(this.transport);

  @override
  List<Object> get props => [transport];

  @override
  String toString() => 'TransportState { $transport }';
}

class TransportInitialState extends TransportState {
  const TransportInitialState(super.transport);

  TransportInitialState copyWith({
    Transport transport,
  }) {
    print('TransportInitialState $transport');
    return TransportInitialState(
      transport ?? this.transport,
    );
  }

  @override
  String toString() => 'TransportInitialState { $transport }';
}

class TransportUpdatedState extends TransportState {
  const TransportUpdatedState(super.transport);

  TransportUpdatedState copyWith({
    Transport transport,
  }) {
    print('TransportUpdatedState $transport');
    return TransportUpdatedState(
      transport ?? this.transport,
    );
  }

  @override
  String toString() => 'TransportUpdatedState state { $transport }';
}
