/// transport_event - BLoC handling AppPage transport events
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'transport_bloc.dart';

/// TransportEvents
/// all are based on Equatable
abstract class TransportEvent extends Equatable {
  const TransportEvent();

  @override
  List<Object> get props => [];
}

/// TransportInitialize has no parameters
class TransportInitialize extends TransportEvent {
  const TransportInitialize();

  @override
  String toString() => 'TransportInitialize event';
}

/// TransportReceivedEvent handles a Transport value.
class TransportReceivedEvent extends TransportEvent {
  final Transport transport;
  const TransportReceivedEvent(this.transport);

  @override
  List<Object> get props => [transport];

  @override
  String toString() => 'TransportReceivedEvent $transport';
}
