/// transport_bloc - BLoC handling Transport events
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../debug.dart';

part 'transport_event.dart';
part 'transport_state.dart';

/// TransportBloc is a singleton Bloc to handle transport state.
/// Transport is a singleton Class that maintains the play/stop/rec_enable/currenttime state.
/// TODO: refactor to use isolate send method
class TransportBloc extends Bloc<TransportEvent, TransportState> {
  static final TransportBloc _singleton = TransportBloc._internal();

  // TransportBloc() : super(TransportInitialState(Transport()));
  factory TransportBloc() => _singleton;
  TransportBloc._internal() : super(TransportInitialState(Transport()));

  @override
  Stream<TransportState> mapEventToState(TransportEvent event) async* {
    dPrint('TransportState mapEventToState $state');

    if (event is TransportReceivedEvent) {
      dPrint('TransportState mapEventToState $state');
      // this change of state needs to happen when the any message is received.
      yield (TransportUpdatedState(event.transport));
    } else if (event is TransportInitialize) {
      yield TransportInitialState(Transport());
    } else {
      dPrint('TransportState mapEventToState unknown event $event $state');
      yield TransportInitialState(Transport());
    }
  }
}
