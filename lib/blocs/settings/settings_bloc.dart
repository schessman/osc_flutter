/// settings_bloc - BLoC handling AppPage settings events
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

// The SettingsBloc handles changes to:
// IP Address/Hostname, UDP Port, and Sessionname.
// When these are changed from Settings or OSC events, the SettingsBloc changes state.

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../debug.dart';
import '../../blocs/blocs.dart';
import '../../data/data.dart';
import '../../osc/osc.dart';

part 'settings_event.dart';
part 'settings_state.dart';

/// SettingsBloc() : singleton Bloc for Settings. Business Logic for Settings
// load settings from persistent storage.
// handle settings updates from UI.
// handle session name changes from OscIsolate.
class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  static final SettingsBloc _singleton = SettingsBloc._internal();
  final SettingsRepository _settingsRepository = SettingsRepository();
  static Settings _settings;

  factory SettingsBloc() => _singleton;

  SettingsBloc._internal() : super(SettingsInitialState(Settings()));

  // SettingsInitializeEvent  - kick it off
  // SettingsUpdatedEvent     - update the Settings
  // SettingsLoadEvent - load the Settings from shared preferences
  @override
  Stream<SettingsState> mapEventToState(SettingsEvent event) async* {
    if (event is SettingsUpdatedEvent) {
      yield* _mapSettingsUpdatedToState(event);
    } else if (event is SettingsLoadEvent) {
      yield* _mapSettingsLoadedToState(event);
    } else if (event is SettingsInitializeEvent) {
      dPrint('SettingsInitializeEvent');
      // wait for Isolate to setup.
      // add(SettingsLoadEvent());
      yield* _mapSettingsLoadedToState(SettingsLoadEvent());
    } else {
      addError(Exception('unsupported event'));
    }
  }

  // set the shared preferences with the repository
  // if changed, notify the UDP isolate the address has changed.
  Stream<SettingsState> _mapSettingsUpdatedToState(
      SettingsUpdatedEvent event) async* {
    try {
      // print('SettingsBloc _mapSettingsUpdatedToState event $event');
      // print('SettingsBloc _settingsRepository $_settingsRepository');
      _settings = await _settingsRepository.updateSettings(event.settings);

      if (state.settings.address != _settings.address ||
          state.settings.port != _settings.port) {
        dPrint(
            'settingsBloc sendInitialMessages _settings $_settings settings ${state.settings}');
        _sendInitialMessages(_settings);
      }

      yield SettingsUpdateSuccess(_settings);
    } catch (e) {
      print('Exception occurs: $e');
      yield SettingsUpdateFailure(event.settings);
    }
  }

  // load the shared preferences from the repository
  // notify the UDP isolate the address has changed.
  Stream<SettingsState> _mapSettingsLoadedToState(
      SettingsLoadEvent event) async* {
    try {
      // print( 'SettingsBloc _mapSettingsLoadedToState _settingsRepository $_settingsRepository');
      _settings = await _settingsRepository.getSettings();
    } catch (e) {
      print('Exception occurs getSettings: $e');
      yield SettingsLoadFailure(null);
    }

    try {
      _sendInitialMessages(_settings);

      yield SettingsLoadSuccess(_settings);
    } catch (e) {
      print('Exception occurs _sendInitialMessages: $e');
      yield SettingsLoadFailure(null);
    }
  }

  /// sendInitialMessages(settings) : notify the isolate to send init messages.
  /// Send the start messages to the OSC system so it will reply with configuration.
  ///
  /// This happens every time the address:port changes as it may be a new system.
  void _sendInitialMessages(settings) async {
    var isolateBloc = IsolateBloc();
    dPrint(
        'settingsBloc sendInitialMessages isolateBloc $isolateBloc settings $settings');

    // clear the current striplist
    StripBloc().add(StripInitialize());
    // set the UDP address and port.
    isolateBloc.add(IsolateSendEvent(SendMsgType.NewAddress, value: settings));

    // prime the pump.  oscHandle.send uses class constants for the message to send.
    isolateBloc.add(IsolateSendEvent(SendMsgType.StripList));
    // isolate maintains ardour object with striptype and feedback values.
    isolateBloc.add(IsolateSendEvent(SendMsgType.SetSurface));
  }

  @override
  String toString() => 'SettingsBloc {settings: $_settings}';
}
