/// settings_event - BLoC handling AppPage settings events
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'settings_bloc.dart';

/// SettingsEvents
// SettingsLoadSuccess - load the settings from shared storage
// SettingsUpdated     - update the settings

/// SettingsEvent() : base class for Settings events
abstract class SettingsEvent extends Equatable {
  const SettingsEvent();

  @override
  List<Object> get props => [];
}

/// SettingsInitializeEvent(): init.
class SettingsInitializeEvent extends SettingsEvent {
  @override
  String toString() => 'SettingsInitializeEvent event';
}

/// SettingsLoadEvent(): load settings from persistent storage.
class SettingsLoadEvent extends SettingsEvent {
  @override
  String toString() => 'SettingsLoadEvent event';
}

/// SettingsUpdatedEvent(settings): update settings
class SettingsUpdatedEvent extends SettingsEvent {
  final Settings settings;

  const SettingsUpdatedEvent(this.settings);

  @override
  List<Object> get props => [settings];

  @override
  String toString() => 'SettingsUpdatedEvent { settings: $settings }';
}
