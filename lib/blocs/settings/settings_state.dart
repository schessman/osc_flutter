/// settings_state - BLoC handling AppPage settings states
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'settings_bloc.dart';

// SettingsState
// The states we define:
// abstract state with final settings
// initial state: default shared preferences
// load in progress: waiting for shared_preferences
// load success: shared preferences loaded
// load failure: shared preferences load failed
// update success: shared preferences updated
// update failure: shared preferences update failed
//
/// SettingsState(settings): base class for Settings states
abstract class SettingsState extends Equatable {
  final Settings settings;
  const SettingsState(this.settings);

  @override
  List<Object> get props => [settings];

  @override
  String toString() => 'SettingsState { settings: $settings }';
}

/// SettingsInitialState(settings): init.
class SettingsInitialState extends SettingsState {
  const SettingsInitialState(super.settings);

  SettingsInitialState copyWith({
    Settings settings,
  }) {
    return SettingsInitialState(
      settings ?? this.settings,
    );
  }

  @override
  String toString() => 'SettingsInitialState { settings: $settings }';
}

/// SettingsLoadFailure(settings): failure state.
class SettingsLoadFailure extends SettingsState {
  const SettingsLoadFailure(super.settings);

  SettingsLoadFailure copyWith({
    Settings settings,
  }) {
    return SettingsLoadFailure(
      settings ?? this.settings,
    );
  }

  @override
  String toString() => 'SettingsLoadFailure state { settings: $settings }';
}

/// SettingsUpdateFailure(settings): failure state.
class SettingsUpdateFailure extends SettingsState {
  const SettingsUpdateFailure(super.settings);

  SettingsUpdateFailure copyWith({
    Settings settings,
  }) {
    return SettingsUpdateFailure(
      settings ?? this.settings,
    );
  }

  @override
  String toString() => 'SettingsUpdateFailure state { settings: $settings }';
}

/// SettingsLoadSuccess(settings): settings loaded successfully.
class SettingsLoadSuccess extends SettingsState {
  const SettingsLoadSuccess(super.settings);

  SettingsLoadSuccess copyWith({
    Settings settings,
  }) {
    return SettingsLoadSuccess(
      settings ?? this.settings,
    );
  }

  @override
  String toString() => 'SettingsLoadSuccess state { settings: $settings }';
}

/// SettingsUpdateSuccess(settings): settings updated successfully.
class SettingsUpdateSuccess extends SettingsState {
  const SettingsUpdateSuccess(super.settings);

  SettingsUpdateSuccess copyWith({
    Settings settings,
  }) {
    return SettingsUpdateSuccess(
      settings ?? this.settings,
    );
  }

  @override
  String toString() => 'SettingsUpdateSuccess state { settings: $settings }';
}
