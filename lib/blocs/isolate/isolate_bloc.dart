/// isolate_bloc
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:bloc/bloc.dart';

import '../../debug.dart';
import '../../osc/osc.dart';

part 'isolate_event.dart';
part 'isolate_state.dart';

/// IsolateBloc(): spawn the UDP I/O isolate and communicate with it.
class IsolateBloc extends Bloc<IsolateEvent, IsolateState> {
  static OSCIsolate _isolate;

  IsolateBloc() : super(IsolateInitial());

  @override
  Stream<IsolateState> mapEventToState(IsolateEvent event) async* {
    dPrint('isolateBloc mapEvent event $event');

    if (event is IsolateInitialize) {
      yield* _mapIsolateInitializeToState(event);
    } else if (event is IsolateSendEvent) {
      yield* _mapIsolateSendEventState(event);
    } else {
      dPrint('isolate_bloc mapEventToState unknown event $event');
      yield IsolateRunningState(null);
    }
  }

  // start the UDP I/O isolate, wait for it, set decode callback
  Stream<IsolateState> _mapIsolateInitializeToState(
      IsolateInitialize event) async* {
    _isolate = await oscComm(
      onData: (message) {
        OscDecode(message);
      },
    );
    dPrint('OSCApp oscHandle $_isolate');
    yield IsolateRunningState(null);
  }

  // send an OSCMessage to the isolate worker.
  Stream<IsolateState> _mapIsolateSendEventState(
      IsolateSendEvent event) async* {
    var msg = SendMsgType(event.type, ssid: event.ssid, value: event.value);
    dPrint('isolateBloc _mapIsolateSendEventState $event $msg');
    _isolate.send(msg);
    yield IsolateRunningState(event.type);
  }
}
