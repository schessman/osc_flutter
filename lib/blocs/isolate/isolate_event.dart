/// isolate_event
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'isolate_bloc.dart';

/// IsolateEvent(): pass messages to and from the UDP Isolate worker.
abstract class IsolateEvent extends Equatable {
  const IsolateEvent();

  @override
  List<Object> get props => [];
}

/// IsolateInitialize(): init.
class IsolateInitialize extends IsolateEvent {
  @override
  String toString() => 'IsolateInitialize event';
}

/// IsolateSendEvent(type, { ssid, value }): SendEvent when UI interaction needs to send OSC message.
/// type is SendMsgType
/// it is possible to have a type without ssid and value.
/// it is possible to have a value without ssid.
/// Value is dynamic, can be null, int, double, bool etc.
class IsolateSendEvent extends IsolateEvent {
  final int type; // SendMsgType
  final int ssid; // optional named parameter
  final dynamic value; // optional named parameter

  const IsolateSendEvent(this.type, {this.ssid, this.value});

  @override
  List<Object> get props => [type, ssid, value];

  @override
  String toString() =>
      'IsolateSendEvent type $type ssid $ssid value ${value.runtimeType}:$value';
}
