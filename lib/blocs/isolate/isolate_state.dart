/// isolate_state
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

part of 'isolate_bloc.dart';

/// IsolateState() : states for the Isolate bloc
abstract class IsolateState extends Equatable {
  const IsolateState();

  @override
  List<Object> get props => [];
}

/// IsolateInitial() : Init.
class IsolateInitial extends IsolateState {
  @override
  String toString() => 'IsolateInitial state';
}

/// IsolateRunningState(type) : normal
class IsolateRunningState extends IsolateState {
  final int type; // SendMsgType

  const IsolateRunningState(this.type);

  @override
  String toString() => 'IsolateRunningState state $type';
}
