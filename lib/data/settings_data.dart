/// settings_data - model and repository for settings
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

import 'package:equatable/equatable.dart';

/// Settings model has Appname, address, port, session
class Settings extends Equatable {
  final String appname;
  final String address;
  final String port;
  final String session;
  static final Settings _singleton = Settings._internal();
  factory Settings() => _singleton;

  // Settings constructor has all optional named parameters
  const Settings._internal({
    appname,
    address,
    port,
    session,
  })  : appname = appname ?? 'OSCFlutter',
        address = address ?? '127.0.0.1',
        port = port ?? '3819',
        session = session ?? 'tbd';

  /// Settings json Constructor from map
  Settings.fromJson(Map<String, dynamic> json)
      : appname = json['appname'],
        address = json['address'],
        port = json['port'],
        session = json['session'];

  /// json Converter for Settings
  Map<String, dynamic> toJson() => {
        'appname': appname,
        'address': address,
        'port': port,
        'session': session,
      };

  Settings copyWith(
      {String appname, String address, String port, String session}) {
    return Settings._internal(
      appname: appname ?? this.appname,
      address: address ?? this.address,
      port: port ?? this.port,
      session: session ?? this.session,
    );
  }

  @override
  List<Object> get props => [appname, address, port, session];

  @override
  String toString() =>
      'Settings {appname: $appname, address: $address, port: $port, session: $session}';
}

/// Singleton Settings Repository uses settings file as backend. json format.
class SettingsRepository {
  static final SettingsRepository _settingsRepository =
      SettingsRepository._internal();

  factory SettingsRepository() {
    return _settingsRepository;
  }

  SettingsRepository._internal();
  final AppSettings _appSettings = AppSettings();

  // update Settings file convert to json
  Future<Settings> updateSettings(Settings settings) async {
    var jsonSettings = jsonEncode(settings);
    print('updateSettings $settings { $jsonSettings }');
    await _appSettings.writeUserSettings(jsonSettings);
    return (settings);
  }

  // get Settings from file convert from json
  Future<Settings> getSettings() async {
    var jsonSettings = await _appSettings.getUserSettings();
    var jsonMap = jsonDecode(jsonSettings);
    print('getSettings $jsonSettings $jsonMap');
    return (Settings.fromJson(jsonMap));
  }

  @override
  String toString() => 'SettingsRepository ${_settingsRepository.hashCode} ';
}

/// File persistence for json strings.  Encoding happens in calling class.
class AppSettings {
  final String settingsFilename = 'OscSettings.json';

  // get local path of documents directory
  Future<String> get _localPath async {
    try {
      final directory = await getApplicationSupportDirectory();
      return directory.path;
    } catch (e) {
      print(e);
    }
    return '/data/user/0/com.example.osc_flutter/files';
  }

  // get the settings file
  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/$settingsFilename');
  }

  // write user setting to file
  Future<File> writeUserSettings(String setting) async {
    final file = await _localFile;
    return file.writeAsString(setting);
  }

  // read the file contents
  Future<String> getUserSettings() async {
    String contents;

    try {
      final file = await _localFile;

      // read the file
      contents = await file.readAsString();
    } catch (e) {
      print(e);
    }

    return contents;
  }
}
