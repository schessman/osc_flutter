/// osc_flutter localization
library;

// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:
//
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FlutterBlocLocalizations {
  static final FlutterBlocLocalizations _singleton =
      FlutterBlocLocalizations._internal();
  FlutterBlocLocalizations._internal();
  static FlutterBlocLocalizations get instance => _singleton;

  Map<dynamic, dynamic> _localisedValues;

  Future<FlutterBlocLocalizations> load(Locale locale) async {
    var jsonContent = await rootBundle
        .loadString('assets/locale/localization_${locale.languageCode}.json');
    _localisedValues = json.decode(jsonContent);
    return this;
  }

  String text(String key) {
    return _localisedValues[key] ?? '$key not found';
  }

  String textArray(String key) {
    return _localisedValues[key].join('\n') ?? '$key not found';
  }
}

class FlutterBlocLocalizationsDelegate
    extends LocalizationsDelegate<FlutterBlocLocalizations> {
  const FlutterBlocLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      ['de', 'en', 'es', 'fr', 'it', 'pt'].contains(locale.languageCode);

  @override
  Future<FlutterBlocLocalizations> load(Locale locale) {
    return FlutterBlocLocalizations.instance.load(locale);
  }

  @override
  bool shouldReload(FlutterBlocLocalizationsDelegate old) => true;
}
