/// All tests for osc_flutter
library;
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

import 'widget_test.dart' as widget_test;
import 'convert_test.dart' as convert_test;

void main() {
  widget_test.main();
  convert_test.main();
}
