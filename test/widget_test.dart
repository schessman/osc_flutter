// This is the osc_flutter widget test driver
// Copyright (C) 2020 Samuel Chessman
// SPDX-License-Identifier: GPL-3.0-or-later
// vim: ts=2 sw=2 noai:

//

import 'package:flutter_test/flutter_test.dart';

import 'package:osc_flutter/ui/ui.dart';

// homepage.dart
// mixer.dart

// settings.dart
void main() {
  // tests for the ui widgets
  testWidgets('SplashPage displays png', (WidgetTester tester) async {
    await tester.pumpWidget(SplashPage());
  });
}
