#!/bin/make
# Copyright (C) 2020 Samuel Chessman
# SPDX-License-Identifier: GPL-3.0-or-later
# vim: ts=2 sw=2 noai:

#
# I like make
DEBUG=--dart-define="DEBUGPRINT=0"
DARTLIST := $(shell find . -name '*.dart' -print)

.PHONY: analyze format linux android test docs dartlist upgrade
all: dartlist analyze format 

therest: upgrade android linux test docs run icons

dartlist:
	find . -name '*.dart' -print | sort > dartlist

icons:
	flutter pub run flutter_launcher_icons:main

analyze: analyze.out

analyze.out: $(DARTLIST) pubspec.lock
	-flutter analyze | grep -v example >& analyze.out

format: format.out

format.out: $(DARTLIST)
	-dart format $(DARTLIST) >& format.out

linux: linux.out

linux.out: $(DARTLIST)
	flutter build -v linux --analyze-size >& linux.out

android:
	flutter build -v apk --target-platform android-arm,android-arm64,android-x64 --split-per-abi >& apk.out

test:
	cd test; dart convert_test.dart
	#flutter run test/convert_test.dart

runx:
	flutter run $(DEBUG) -d linux |& tee runx.out

run:
	flutter run $(DEBUG) |& tee run.out

docs:
	-rm -rf doc/api
	FLUTTER_ROOT='/opt/flutter' dartdoc --ignore unresolved-doc-reference |& tee doc.out

upgrade:
	flutter --version
	flutter upgrade
	flutter pub upgrade

