# osc\_flutter
GPL3+ Flutter/Dart App to control Ardour6 via OSC
(https://en.wikipedia.org/wiki/Open_Sound_Control) for live mixing and recording.

## Platforms
This app targets Android 8.1.0 (API 22) or greater.  It works on my Nexus 7 (2012) tablet with LineageOS running Android 8.1.0 (API 27).
This app needs to connect to Ardour6 with OSC enabled running on another system.  I use RPi3 running Linux.  See https://audiopi.blogspot.com/

## License
### Copyright (C) 2020, Samuel S Chessman
### SPDX-License-Identifier: GPL-3.0-or-later
The sources are found at https://gitlab.com/schessman/osc_flutter/

## Credits
This app is an original work by Sam Chessman based on the Dart and Flutter tutorials found online.

### The lib/src/ files are based on the dart OSC library https://github.com/pq/osc with changes for Ardour.
### The lib/widget/led.dart file is based on https://github.com/ltdangkhoa/Flutter-Animation-Progress-Bar.  See LICENSE.led.
### The lib/widget/slider.dart file is based on https://github.com/Ali-Azmoud/flutter_xlider.  See LICENSE.slider.
 
## Files

* LICENSE.led - license for led widget
* LICENSE - License for distribution
* LICENSE.slider - license for slider widget
* README.md - this file, basic overview and instructions.
* README_msg.md - out of date documentation of interisolate messages
* analysis_options.yaml - pedantic configuration
* android - directory with Android sources
* assets - directory with translations and icons
* doc - dartdoc html pages
* example - directory with OSC test programs
* ios - directory with iOS sources etc.
* lib - directory with common sources etc.
* linux - flutter directory for linux support
* Makefile - mature software build configuration
* osc\_flutter.iml - flutter config XML
* pubspec.lock - locked version dependencies
* pubspec.yaml - flutter/dart dependency specification
* test - directory with test code

## Todo
* setup tests
* add license page
* swipe drawers left/right
* validate translations
* splash page
