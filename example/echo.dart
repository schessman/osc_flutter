import 'package:osc_flutter/osclib.dart';

const int defaultPort = 8000;

/// simple echo server; useful for testing.
void main(List<String> args) async {
  final port = args.length == 1 ? int.parse(args[0]) : defaultPort;

  print('echo osc listening on port $port... (^C to quit)');

  final socket = OSCSocket(serverPort: port);
  await socket.setup();
  await socket.listen((msg) {
    print('received: $msg from $socket');
    // socket.reply(OSCMessage('/received', arguments: []));
  });
}
