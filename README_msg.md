# osc\_flutter InterIsolate messaging

## License
### Copyright (C) 2020, Samuel S Chessman
### SPDX-License-Identifier: GPL-3.0-or-later
The sources are found at https://gitlab.com/schessman/osc_flutter/

## Primary Isolate runs UI
As is normal in Dart/Flutter, the main event loop handles the UI events.  As OSC handling could block or delay the UI,
it is handled by an OSC Isolate, with messages sent to and received from the secondary Isolate using the standard SendPort/ReceivePort interface.
Messages received from the OSC Isolate will affect the UI screens/widgets and may arrive in large quantities.  See meter display.
Messages sent from the UI Isolate are generated by human interaction, or initialization, and are not high volume.

## Secondary Isolate runs OSC interface
The encoding/decoding of OSC messages and sending/receiving UDP datagrams is handled in a secondary Dart isolate.
Messages received from the UI are encoded into OSC format and sent with UDP to the network.
UDP Messages received from the OSC system are decoded into Dart format and sent to the UI Isolate.

## Messages from UI to OSC
The messages sent from the UI isolate to the OSC isolate consist of
o initialize '/strip/list'
o initialize '/set_surface bank_size strip_types feedback fadermode send_page_size plugin_page_size'
o begin '/goto_start'
o end '/goto_end'
o play '/transport_play'
o rec '/rec_enable_toggle'
o stop '/transport_stop'

## Messages from OSC to UI
The messages sent from the UI isolate to the OSC isolate consist of
o bank_down '/bank_down'
o bank_up '/bank_up'
o cancel_all_solos '/cancel_all_solos'
o ffwd '/ffwd'
o loop_toggle '/loop_toggle'
o master_fader '/master/fader'
o master_gain '/Manual'
o master_gain '/master/gain'
o master_meter '/master/meter'
o master_mute '/master/mute'
o master_name '/master/name'
o master_pan_stereo_position '/master/pan_stereo_position'
o master_signal '/master/signal'
o master_trimdb '/master/trimdB'
o monitor_name '/monitor/name'
o position_smpte '/position/smpte'
o position_time '/position/time'
o rec_enable_toggle '/rec_enable_toggle'
o record_tally '/record_tally'
o reply '/reply'
o rewind '/rewind'
o select_comment '/select/comment'
o select_comp_enable '/select/comp_enable'
o select_comp_makeup '/select/comp_makeup'
o select_comp_mode '/select/comp_mode'
o select_comp_mode_name '/select/comp_mode_name'
o select_comp_speed '/select/comp_speed'
o select_comp_speed_name '/select/comp_speed_name'
o select_comp_threshold '/select/comp_threshold'
o select_expand '/select/expand'
o select_fader '/select/fader'
o select_fader_automation '/select/fader/automation'
o select_fader_automation_name '/select/fader/automation_name'
o select_gain '/select/gain'
o select_gain_automation '/select/gain/automation'
o select_gain_automation_name '/select/gain/automation_name'
o select_meter '/select/meter'
o select_monitor_disk '/select/monitor_disk'
o select_monitor_input '/select/monitor_input'
o select_mute '/select/mute'
o select_n_inputs '/select/n_inputs'
o select_n_outputs '/select/n_outputs'
o select_name '/select/name'
o select_pan_elevation_position '/select/pan_elevation_position'
o select_pan_frontback_position '/select/pan_frontback_position'
o select_pan_lfe_control '/select/pan_lfe_control'
o select_pan_stereo_position '/select/pan_stereo_position'
o select_pan_stereo_width '/select/pan_stereo_width'
o select_plugin_name '/select/plugin/name'
o select_plugin_parameter '/select/plugin/parameter'
o select_plugin_parameter_name '/select/plugin/parameter/name'
o select_polarity '/select/polarity'
o select_recenable '/select/recenable'
o select_record_safe '/select/record_safe'
o select_select '/select/select'
o select_signal '/select/signal'
o select_solo '/select/solo'
o select_solo_iso '/select/solo_iso'
o select_solo_safe '/select/solo_safe'
o select_trimdB '/select/trimdB'
o session_name '/session_name'
o strip_expand '/strip/expand'
o strip_fader '/strip/fader'
o strip_fader_automation '/strip/fader/automation'
o strip_fader_automation_name '/strip/fader/automation_name'
o strip_gain '/strip/gain'
o strip_gain_automation '/strip/gain/automation'
o strip_gain_automation_name '/strip/gain/automation_name'
o strip_gui_select '/strip/gui_select'
o strip_list '/strip/list'
o strip_meter '/strip/meter'
o strip_monitor_disk '/strip/monitor_disk'
o strip_monitor_input '/strip/monitor_input'
o strip_mute '/strip/mute'
o strip_name '/strip/name'
o strip_pan_stereo_position '/strip/pan_stereo_position'
o strip_recenable '/strip/recenable'
o strip_record_safe '/strip/record_safe'
o strip_select '/strip/select'
o strip_signal '/strip/signal'
o strip_solo '/strip/solo'
o strip_solo_iso '/strip/solo_iso'
o strip_solo_safe '/strip/solo_safe'
o strip_trimdb '/strip/trimdB'
o transport_play '/transport_play'
o transport_stop '/transport_stop'
